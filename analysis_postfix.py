import pandas as pd
import numpy as np
import re
from datetime import datetime

script_version = "4.3"
post_version = "20"
print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("outputs/train.{0}.csv".format(script_version))
train["before"] = train["before"].astype(str)
train["prediction"] = train["prediction"].astype(str)
train["after"] = train["after"].astype(str)
train["class_pred"] = train["class_pred"].astype(float).astype(int)

unsolved = pd.DataFrame.from_csv("outputs/not_solved_{0}.csv".format(post_version))
unsolved["before"] = unsolved["before"].astype(str)

test = pd.read_csv("outputs/test.{0}.csv".format(script_version), dtype=str)
test["before"] = test["before"].astype(str)
test["prediction"] = test["prediction"].astype(str)
test["class_pred"] = test["class_pred"].astype(float).astype(int)

my_class = "PLAIN"

print(unsolved)
print(unsolved["class"].value_counts())
print(unsolved["before"].value_counts())
tmp = zip(unsolved["before"][unsolved["class"] == my_class].values.tolist(),
          unsolved["after"][unsolved["class"] == my_class].values.tolist(),
          unsolved["prediction"][unsolved["class"] == my_class].values.tolist()
          )
for a, b, c in tmp:
    print("###")
    print(a)
    print(b)
    print(c)

filtered_unsolved = unsolved[unsolved["class"] == "PLAIN"]
vc = filtered_unsolved["before"].value_counts()
print(vc)

for index_i in vc.index.values[:50]:
    print("***")
    print(index_i)
    print(train["after"][train["before"] == index_i].value_counts())

# for index_i in filtered_unsolved.index.values[:50]:
#     sentence_index_i = train["sentence_id"].at[index_i]
#     print(" ".join(train["before"][train["sentence_id"] == sentence_index_i].values.tolist()))
#     print(" ".join(train["after"][train["sentence_id"] == sentence_index_i].values.tolist()))
#
# filtered_unsolved = unsolved[np.logical_and(unsolved["class"] == "DIGIT",
#                                             unsolved["before"].map(lambda x: bool(re.fullmatch("[0-9]+", x))))]
# print("#############################")
# for index_i in filtered_unsolved.index.values[:50]:
#     sentence_index_i = train["sentence_id"].at[index_i]
#     print(" ".join(train["before"][train["sentence_id"] == sentence_index_i].values.tolist()))
#     print(" ".join(train["after"][train["sentence_id"] == sentence_index_i].values.tolist()))
#
# print(unsolved["before"][unsolved["class"] == "MEASURE"].values.tolist())
# print(unsolved["before"][unsolved["class"] == "CARDINAL"].values.tolist())
# print(unsolved["before"][unsolved["class"] == "DIGIT"].values.tolist())
