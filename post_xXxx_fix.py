import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime

import word_parser
import plain_helpers
import dict_handler

import pyximport

pyximport.install()
import word_context


def fix_caps_and_lowers(train: pd.DataFrame, test: pd.DataFrame, evaluation: pd.DataFrame, verbose=False):
    re_exp = "[A-Z]*[a-zé]+[A-Z]+[a-zéA-Z]*[ ,-]*"
    train_local_df = train[train["before"].map(lambda x: bool(re.fullmatch(re_exp, x)))]
    eval_local_df = evaluation[evaluation["before"].map(lambda x: bool(re.fullmatch(re_exp, x)))]
    test_local_df = test[test["before"].map(lambda x: bool(re.fullmatch(re_exp, x)))]

    def sum_errors(df):
        return np.sum(df["after"] != df["prediction"])

    print(sum_errors(train_local_df))
    if verbose:
        print(train_local_df["class_pred"].value_counts())

    train_local_df["letters_target"] = 0
    for i in range(train_local_df.shape[0]):
        if train_local_df["after"].iat[i] == word_parser.parse_letters(train_local_df["before"].iat[i]):
            train_local_df["letters_target"].iat[i] = 0
        elif train_local_df["after"].iat[i] == word_parser.parse_plain(train_local_df["before"].iat[i]):
            train_local_df["letters_target"].iat[i] = 1
        else:
            train_local_df["letters_target"].iat[i] = 2

    eval_local_df["letters_target"] = 0
    for i in range(eval_local_df.shape[0]):
        if eval_local_df["after"].iat[i] == word_parser.parse_letters(eval_local_df["before"].iat[i]):
            eval_local_df["letters_target"].iat[i] = 0
        elif eval_local_df["after"].iat[i] == word_parser.parse_plain(eval_local_df["before"].iat[i]):
            eval_local_df["letters_target"].iat[i] = 1
        else:
            eval_local_df["letters_target"].iat[i] = 2

    if verbose:
        print(train_local_df["letters_target"].value_counts())

    def print_sentences(sentences_id_arr, n):
        print("There are %d words" % sentences_id_arr.size)
        n = min(n, sentences_id_arr.size)
        for i in range(n):
            sentence = train[train["sentence_id"] == sentences_id_arr[i]]
            words = sentence["before"].values.tolist()
            words_after = sentence["after"].values.tolist()
            class_pred = sentence["class_pred"].values.astype(str).tolist()
            print(i)
            print(" ".join(words))
            print(" ".join(words_after))
            print(" ".join(class_pred))

    n_sentences = 10
    if verbose:
        print("\nPrinting 2 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_local_df["sentence_id"][train_local_df["letters_target"] == 2].values, n_sentences)
        print("\nPrinting 1 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_local_df["sentence_id"][train_local_df["letters_target"] == 1].values, n_sentences)
        print("\nPrinting 0 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_local_df["sentence_id"][train_local_df["letters_target"] == 0].values, n_sentences)

    def neigh_classes(full_df, df):
        df["before_class"] = -1
        df["before2_class"] = -1
        df["after_class"] = -1
        df["after2_class"] = -1
        indices = df.index.values
        tmp = full_df[["class_pred", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 2 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 2, 1]:
                    df["before2_class"].iat[i] = tmp[index_i - 2, 0]
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    df["before_class"].iat[i] = tmp[index_i - 1, 0]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    df["after_class"].iat[i] = tmp[index_i + 1, 0]
            if index_i + 2 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 2, 1]:
                    df["after2_class"].iat[i] = tmp[index_i + 2, 0]
        return df

    def neigh_number(full_df, df):
        df["before_number"] = -1
        df["after_number"] = -1
        indices = df.index.values
        tmp = full_df[["before", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i - 1, 0]):
                        df["before_number"].iat[i] = int(tmp[index_i - 1, 0])
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i + 1, 0]):
                        df["after_number"].iat[i] = int(tmp[index_i + 1, 0])
        return df

    train_local_df = neigh_classes(train, train_local_df)
    eval_local_df = neigh_classes(evaluation, eval_local_df)
    test_local_df = neigh_classes(test, test_local_df)

    train_local_df = neigh_number(train, train_local_df)
    eval_local_df = neigh_number(evaluation, eval_local_df)
    test_local_df = neigh_number(test, test_local_df)

    train_local_df["length"] = train_local_df["before"].map(lambda x: len(x))
    eval_local_df["length"] = eval_local_df["before"].map(lambda x: len(x))
    test_local_df["length"] = test_local_df["before"].map(lambda x: len(x))

    train_local_df["n_ouaei"] = train_local_df["before"].map(lambda x: len(re.findall("[OUAEIouaei]", x)))
    eval_local_df["n_ouaei"] = eval_local_df["before"].map(lambda x: len(re.findall("[OUAEIouaei]", x)))
    test_local_df["n_ouaei"] = test_local_df["before"].map(lambda x: len(re.findall("[OUAEIouaei]", x)))

    train_local_df["isJoined"] = train_local_df["before"].map(lambda x: len(re.findall("[A-Z][a-z]+([A-Z][a-z]+)+", x)))
    eval_local_df["isJoined"] = eval_local_df["before"].map(lambda x: len(re.findall("[A-Z][a-z]+([A-Z][a-z]+)+", x)))
    test_local_df["isJoined"] = test_local_df["before"].map(lambda x: len(re.findall("[A-Z][a-z]+([A-Z][a-z]+)+", x)))

    train_local_df["percent_ouaei"] = train_local_df["n_ouaei"].values / train_local_df["length"].values
    eval_local_df["percent_ouaei"] = eval_local_df["n_ouaei"].values / eval_local_df["length"].values
    test_local_df["percent_ouaei"] = test_local_df["n_ouaei"].values / test_local_df["length"].values

    print("Plain dict analysis ", datetime.now().strftime("%H:%M:%S"))
    plain_vc = dict_handler.create_vc_series("outputs/plain.csv", re_exp, 100000, 1000000000)
    train_local_df["plainDict"] = word_context.search_sorted_series(
        train_local_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    eval_local_df["plainDict"] = word_context.search_sorted_series(
        eval_local_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    test_local_df["plainDict"] = word_context.search_sorted_series(
        test_local_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)

    print("Letters dict analysis ", datetime.now().strftime("%H:%M:%S"))
    letters_vc = dict_handler.create_vc_series("outputs/letters.csv", re_exp, 100000)
    train_local_df["lettersDict"] = word_context.search_sorted_series(
        train_local_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    eval_local_df["lettersDict"] = word_context.search_sorted_series(
        eval_local_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    test_local_df["lettersDict"] = word_context.search_sorted_series(
        test_local_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)

    train_local_df["difference"] = train_local_df["plainDict"].values - train_local_df["lettersDict"].values
    eval_local_df["difference"] = eval_local_df["plainDict"].values - eval_local_df["lettersDict"].values
    test_local_df["difference"] = test_local_df["plainDict"].values - test_local_df["lettersDict"].values

    train_local_df["hasJunc"] = train_local_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1))
    eval_local_df["hasJunc"] = eval_local_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1))
    test_local_df["hasJunc"] = test_local_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1))

    xg_train = xgb.DMatrix(
        train_local_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class",
                        "length", "token_id", "n_ouaei", "percent_ouaei", "difference", "hasJunc",
                        "plainDict", "lettersDict", "isJoined"]].values,
        label=train_local_df["letters_target"].values)
    xg_eval = xgb.DMatrix(
        eval_local_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class",
                       "length", "token_id", "n_ouaei", "percent_ouaei", "difference", "hasJunc",
                       "plainDict", "lettersDict", "isJoined"]].values,
        label=eval_local_df["letters_target"].values)
    xg_test = xgb.DMatrix(
        test_local_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class",
                       "length", "token_id", "n_ouaei", "percent_ouaei", "difference", "hasJunc",
                       "plainDict", "lettersDict", "isJoined"]].values)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'multi:softmax'
    param['eval_metric'] = 'merror'
    # scale weight of positive examples
    param['eta'] = 0.03
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4
    param['num_class'] = 3

    watchlist = [(xg_train, 'train'), (xg_eval, 'eval')]
    num_round = 300
    bst = xgb.train(param, xg_train, num_round, watchlist)

    # get prediction
    pred_train = bst.predict(xg_train)
    pred_test = bst.predict(xg_test)

    print(np.sum(pred_train != train_local_df["letters_target"]))

    if verbose:
        print(train_local_df[["before", "after", "prediction", "letters_target"]][pred_train !=
                                                                                  train_local_df["letters_target"]])

        wrong_prediction = train_local_df[pred_train != train_local_df["letters_target"]]
        print("\nPrinting date sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 2].values, n_sentences)
        print("\nPrinting digits sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 1].values, n_sentences)
        print("\nPrinting cardinal sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 0].values, n_sentences)

    train_indices = train_local_df.index.values
    test_indices = test_local_df.index.values
    print("Fixing")
    for i, index_i in enumerate(train_indices):
        if pred_train[i] == 0:
            train["prediction"].iat[index_i] = word_parser.parse_letters(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 7
        elif pred_train[i] == 1:
            train["prediction"].iat[index_i] = word_parser.parse_plain(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 11
    for i, index_i in enumerate(test_indices):
        if pred_test[i] == 0:
            test["prediction"].iat[index_i] = word_parser.parse_letters(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 7
        elif pred_test[i] == 1:
            test["prediction"].iat[index_i] = word_parser.parse_plain(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 11

    return train, test
