import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime

import word_parser
from plain_helpers import is_capital_stuck
import dict_handler

import pyximport

pyximport.install()
import word_context


def fix_caps(train: pd.DataFrame, test: pd.DataFrame, evaluation: pd.DataFrame, verbose=False):
    train = is_capital_stuck(train)
    local_evaluation = is_capital_stuck(evaluation)
    test = is_capital_stuck(test)

    train_caps_df = train[train["before"].map(lambda x: bool(re.fullmatch("[A-Z][A-Z']*s?[ ,-]*", x)))]
    eval_caps_df = local_evaluation[
        local_evaluation["before"].map(lambda x: bool(re.fullmatch("[A-Z][A-Z']*s?[ ,-]*", x)))]
    test_caps_df = test[test["before"].map(lambda x: bool(re.fullmatch("[A-Z][A-Z']*s?[ ,-]*", x)))]

    not_vowel = "([bcdfghjklmnpqrstvwxyz]|th|st|ch|sh|ght?|ss)"
    vowel = "([auioeé]|ea|ou|ee|ai|oa|ui|ei|ie|ue)"
    string_filter = not_vowel + "?" + "(" + vowel + not_vowel + "|i[oa]n)+[éey]?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_caps_df["re_filter1a"] = train_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_caps_df["re_filter1a"] = eval_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_caps_df["re_filter1a"] = test_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    string_filter = not_vowel + "(" + vowel + not_vowel + "|i[oa]n)+[éey]?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_caps_df["re_filter1b"] = train_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_caps_df["re_filter1b"] = eval_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_caps_df["re_filter1b"] = test_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    string_filter = not_vowel + "{1,2}" + "(" + vowel + not_vowel + "{1,2}" + "|i[oa]n)+([éeys]|ies)?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_caps_df["re_filter2a"] = train_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_caps_df["re_filter2a"] = eval_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_caps_df["re_filter2a"] = test_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    string_filter = not_vowel + "{0,2}" + "(" + vowel + not_vowel + "{1,2}" + "|i[oa]n)+([éeys]|ies)?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_caps_df["re_filter2b"] = train_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_caps_df["re_filter2b"] = eval_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_caps_df["re_filter2b"] = test_caps_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    train_caps_df["hasS"] = train_caps_df["before"].map(lambda x: int("'s" in x or x[-1] == "s"))
    eval_caps_df["hasS"] = eval_caps_df["before"].map(lambda x: int("'s" in x or x[-1] == "s"))
    test_caps_df["hasS"] = test_caps_df["before"].map(lambda x: int("'s" in x or x[-1] == "s"))

    train_caps_df["hasPsik"] = train_caps_df["before"].map(lambda x: int("'" in x))
    eval_caps_df["hasPsik"] = eval_caps_df["before"].map(lambda x: int("'" in x))
    test_caps_df["hasPsik"] = test_caps_df["before"].map(lambda x: int("'" in x))

    def sum_errors(df):
        return np.sum(df["after"] != df["prediction"])

    print(sum_errors(train_caps_df))
    if verbose:
        print(train_caps_df["class_pred"].value_counts())

    train_caps_df["letters_target"] = 0
    for i in range(train_caps_df.shape[0]):
        if train_caps_df["after"].iat[i] == word_parser.parse_letters(train_caps_df["before"].iat[i]):
            train_caps_df["letters_target"].iat[i] = 0
        elif train_caps_df["after"].iat[i] == word_parser.parse_plain(train_caps_df["before"].iat[i]):
            train_caps_df["letters_target"].iat[i] = 1
        else:
            train_caps_df["letters_target"].iat[i] = 2

    eval_caps_df["letters_target"] = 0
    for i in range(eval_caps_df.shape[0]):
        if eval_caps_df["after"].iat[i] == word_parser.parse_letters(eval_caps_df["before"].iat[i]):
            eval_caps_df["letters_target"].iat[i] = 0
        elif eval_caps_df["after"].iat[i] == word_parser.parse_plain(eval_caps_df["before"].iat[i]):
            eval_caps_df["letters_target"].iat[i] = 1
        else:
            eval_caps_df["letters_target"].iat[i] = 2

    if verbose:
        print(train_caps_df["letters_target"].value_counts())
        print(eval_caps_df["letters_target"].value_counts())

    def print_sentences(sentences_id_arr, n):
        print("There are %d words" % sentences_id_arr.size)
        n = min(n, sentences_id_arr.size)
        for i in range(n):
            sentence = train[train["sentence_id"] == sentences_id_arr[i]]
            words = sentence["before"].values.tolist()
            words_after = sentence["after"].values.tolist()
            class_pred = sentence["class_pred"].values.astype(str).tolist()
            print(i)
            print(" ".join(words))
            print(" ".join(words_after))
            print(" ".join(class_pred))

    n_sentences = 10
    if verbose:
        print("\nPrinting 2 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_caps_df["sentence_id"][train_caps_df["letters_target"] == 2].values, n_sentences)
        print("\nPrinting 1 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_caps_df["sentence_id"][train_caps_df["letters_target"] == 1].values, n_sentences)
        print("\nPrinting 0 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_caps_df["sentence_id"][train_caps_df["letters_target"] == 0].values, n_sentences)

    def neigh_classes(full_df, df):
        df["before_class"] = -1
        df["before2_class"] = -1
        df["after_class"] = -1
        df["after2_class"] = -1
        indices = df.index.values
        tmp = full_df[["class_pred", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 2 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 2, 1]:
                    df["before2_class"].iat[i] = tmp[index_i - 2, 0]
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    df["before_class"].iat[i] = tmp[index_i - 1, 0]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    df["after_class"].iat[i] = tmp[index_i + 1, 0]
            if index_i + 2 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 2, 1]:
                    df["after2_class"].iat[i] = tmp[index_i + 2, 0]
        return df

    def neigh_number(full_df, df):
        df["before_number"] = -1
        df["after_number"] = -1
        indices = df.index.values
        tmp = full_df[["before", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i - 1, 0]):
                        df["before_number"].iat[i] = int(tmp[index_i - 1, 0])
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i + 1, 0]):
                        df["after_number"].iat[i] = int(tmp[index_i + 1, 0])
        return df

    def before_x(full_df, df):
        before_letters_series = pd.Series.from_csv("outputs/before_letters.csv")
        before_letters_index = before_letters_series.index.values
        after_letters_series = pd.Series.from_csv("outputs/after_letters.csv")
        after_letters_index = after_letters_series.index.values
        df["before_letters"] = 0
        df["after_letters"] = 0
        indices = df.index.values
        tmp = full_df["before"].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i - 1].lower() in before_letters_index:
                    df["before_letters"].iat[i] = before_letters_series[tmp[index_i - 1].lower()]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i + 1].lower() in after_letters_index:
                    df["after_letters"].iat[i] = after_letters_series[tmp[index_i + 1].lower()]
        return df

    train_caps_df = neigh_classes(train, train_caps_df)
    eval_caps_df = neigh_classes(local_evaluation, eval_caps_df)
    test_caps_df = neigh_classes(test, test_caps_df)

    train_caps_df = before_x(train, train_caps_df)
    eval_caps_df = before_x(local_evaluation, eval_caps_df)
    test_caps_df = before_x(test, test_caps_df)

    train_caps_df = neigh_number(train, train_caps_df)
    eval_caps_df = neigh_number(local_evaluation, eval_caps_df)
    test_caps_df = neigh_number(test, test_caps_df)

    train_caps_df["roman"] = train_caps_df["before"].map(lambda x: bool(re.fullmatch("[IVXLCDM]+", x)))
    eval_caps_df["roman"] = eval_caps_df["before"].map(lambda x: bool(re.fullmatch("[IVXLCDM]+", x)))
    test_caps_df["roman"] = test_caps_df["before"].map(lambda x: bool(re.fullmatch("[IVXLCDM]+", x)))

    roman_values = {'M': 1000, 'D': 500, 'C': 100, 'L': 50,
                    'X': 10, 'V': 5, 'I': 1}

    def roman_to_int(string):
        """Convert from Roman numerals to an integer."""
        if bool(re.fullmatch("[IVXLCDM]+", string)):
            roman = []
            for char in string:
                if char in roman_values:
                    roman.append(char)
            numbers = [roman_values[char] for char in roman]
            total = 0
            if len(numbers) > 1:
                for num1, num2 in zip(numbers, numbers[1:]):
                    if num1 >= num2:
                        total += num1
                    else:
                        total -= num1
                return total + num2
            else:
                return numbers[0]
        else:
            return -1

    train_caps_df["roman_value"] = train_caps_df["before"].map(lambda x: roman_to_int(x))
    eval_caps_df["roman_value"] = eval_caps_df["before"].map(lambda x: roman_to_int(x))
    test_caps_df["roman_value"] = test_caps_df["before"].map(lambda x: roman_to_int(x))

    num_map = [(1000, 'M'), (900, 'CM'), (500, 'D'), (400, 'CD'), (100, 'C'), (90, 'XC'),
               (50, 'L'), (40, 'XL'), (10, 'X'), (9, 'IX'), (5, 'V'), (4, 'IV'), (1, 'I')]

    def num2roman(num):
        if num > 0:
            roman = ''

            while num > 0:
                for i, r in num_map:
                    while num >= i:
                        roman += r
                        num -= i

            return roman
        return ""

    train_caps_df["roman_valid"] = 0
    eval_caps_df["roman_valid"] = 0
    test_caps_df["roman_valid"] = 0

    for i in range(train_caps_df.shape[0]):
        if num2roman(train_caps_df["roman_value"].iat[i]) == train_caps_df["before"].iat[i]:
            train_caps_df["roman_valid"].iat[i] = 1

    for i in range(eval_caps_df.shape[0]):
        if num2roman(eval_caps_df["roman_value"].iat[i]) == eval_caps_df["before"].iat[i]:
            eval_caps_df["roman_valid"].iat[i] = 1

    for i in range(test_caps_df.shape[0]):
        if num2roman(test_caps_df["roman_value"].iat[i]) == test_caps_df["before"].iat[i]:
            test_caps_df["roman_valid"].iat[i] = 1

    train_caps_df["length"] = train_caps_df["before"].map(lambda x: len(x))
    eval_caps_df["length"] = eval_caps_df["before"].map(lambda x: len(x))
    test_caps_df["length"] = test_caps_df["before"].map(lambda x: len(x))

    train_caps_df["n_ouaei"] = train_caps_df["before"].map(lambda x: len(re.findall("[OUAEI]+", x)))
    eval_caps_df["n_ouaei"] = eval_caps_df["before"].map(lambda x: len(re.findall("[OUAEI]+", x)))
    test_caps_df["n_ouaei"] = test_caps_df["before"].map(lambda x: len(re.findall("[OUAEI]+", x)))

    train_caps_df["percent_ouaei"] = train_caps_df["n_ouaei"].values / train_caps_df["length"].values
    eval_caps_df["percent_ouaei"] = eval_caps_df["n_ouaei"].values / eval_caps_df["length"].values
    test_caps_df["percent_ouaei"] = test_caps_df["n_ouaei"].values / test_caps_df["length"].values

    print("Plain dict analysis ", datetime.now().strftime("%H:%M:%S"))
    plain_vc = dict_handler.create_vc_series("outputs/plain.csv", "[A-Z][A-Z']*s?", 200000, 1000000000)
    train_caps_df["plainDict"] = word_context.search_sorted_series(
        train_caps_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    eval_caps_df["plainDict"] = word_context.search_sorted_series(
        eval_caps_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    test_caps_df["plainDict"] = word_context.search_sorted_series(
        test_caps_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)

    print("Plain dict analysis ", datetime.now().strftime("%H:%M:%S"))
    plain_vc = dict_handler.create_vc_series("outputs/plain.csv", "[a-z][a-z']*s?", 200000, 1000000000)
    train_caps_df["plainLowerDict"] = word_context.search_sorted_series(
        train_caps_df["before"].map(lambda x: x.lower()).values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    eval_caps_df["plainLowerDict"] = word_context.search_sorted_series(
        eval_caps_df["before"].map(lambda x: x.lower()).values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    test_caps_df["plainLowerDict"] = word_context.search_sorted_series(
        test_caps_df["before"].map(lambda x: x.lower()).values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)

    print("Letters dict analysis ", datetime.now().strftime("%H:%M:%S"))
    letters_vc = dict_handler.create_vc_series("outputs/letters.csv", "[A-Z][A-Z']*s?", 200000)
    train_caps_df["lettersDict"] = word_context.search_sorted_series(
        train_caps_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    eval_caps_df["lettersDict"] = word_context.search_sorted_series(
        eval_caps_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    test_caps_df["lettersDict"] = word_context.search_sorted_series(
        test_caps_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)

    print("Letters dict analysis ", datetime.now().strftime("%H:%M:%S"))
    letters_vc = dict_handler.create_vc_series("outputs/letters.csv", "[a-z][a-z']*s?", 200000)
    train_caps_df["lettersLowerDict"] = word_context.search_sorted_series(
        train_caps_df["before"].map(lambda x: x.lower()).values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    eval_caps_df["lettersLowerDict"] = word_context.search_sorted_series(
        eval_caps_df["before"].map(lambda x: x.lower()).values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    test_caps_df["lettersLowerDict"] = word_context.search_sorted_series(
        test_caps_df["before"].map(lambda x: x.lower()).values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)

    train_caps_df["difference"] = train_caps_df["plainDict"].values - train_caps_df["lettersDict"].values
    eval_caps_df["difference"] = eval_caps_df["plainDict"].values - eval_caps_df["lettersDict"].values
    test_caps_df["difference"] = test_caps_df["plainDict"].values - test_caps_df["lettersDict"].values

    train_caps_df["differenceLower"] = train_caps_df["plainLowerDict"].values - train_caps_df["lettersLowerDict"].values
    eval_caps_df["differenceLower"] = eval_caps_df["plainLowerDict"].values - eval_caps_df["lettersLowerDict"].values
    test_caps_df["differenceLower"] = test_caps_df["plainLowerDict"].values - test_caps_df["lettersLowerDict"].values

    train_caps_df["hasJunc"] = train_caps_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1))
    eval_caps_df["hasJunc"] = eval_caps_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1))
    test_caps_df["hasJunc"] = test_caps_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1))

    print(train_caps_df[["plainLowerDict", "lettersLowerDict", "differenceLower"]])
    print("Starting classifing", datetime.now().strftime("%H:%M:%S"))
    xg_train = xgb.DMatrix(
        train_caps_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "isCapital_locked",
                       "length", "roman", "token_id", "roman_value", "roman_valid", "n_ouaei", "percent_ouaei", "hasS",
                       "plainDict", "lettersDict", "difference", "hasJunc", "before_letters", "after_letters",
                       "plainLowerDict", "lettersLowerDict", "differenceLower", "hasPsik",
                       "re_filter1a", "re_filter1b", "re_filter2a", "re_filter2b"]].values,
        label=train_caps_df["letters_target"].values)
    xg_eval = xgb.DMatrix(
        eval_caps_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "isCapital_locked",
                      "length", "roman", "token_id", "roman_value", "roman_valid", "n_ouaei", "percent_ouaei", "hasS",
                      "plainDict", "lettersDict", "difference", "hasJunc", "before_letters", "after_letters",
                      "plainLowerDict", "lettersLowerDict", "differenceLower", "hasPsik",
                      "re_filter1a", "re_filter1b", "re_filter2a", "re_filter2b"]].values,
        label=eval_caps_df["letters_target"].values)

    xg_test = xgb.DMatrix(
        test_caps_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "isCapital_locked",
                      "length", "roman", "token_id", "roman_value", "roman_valid", "n_ouaei", "percent_ouaei", "hasS",
                      "plainDict", "lettersDict", "difference", "hasJunc", "before_letters", "after_letters",
                      "plainLowerDict", "lettersLowerDict", "differenceLower", "hasPsik",
                      "re_filter1a", "re_filter1b", "re_filter2a", "re_filter2b"]].values)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'multi:softmax'
    param['eval_metric'] = 'merror'
    # scale weight of positive examples
    param['eta'] = 0.03
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4
    param['num_class'] = 3

    watchlist = [(xg_train, 'train'), (xg_eval, 'eval')]
    num_round = 800
    bst = xgb.train(param, xg_train, num_round, watchlist)

    # get prediction
    pred_train = bst.predict(xg_train)
    pred_test = bst.predict(xg_test)

    print(np.sum(pred_train != train_caps_df["letters_target"]))

    if verbose:
        print(train_caps_df[["before", "after", "prediction", "letters_target"]][pred_train !=
                                                                                 train_caps_df["letters_target"]])

        wrong_prediction = train_caps_df[pred_train != train_caps_df["letters_target"]]
        print("\nPrinting date sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 2].values, n_sentences)
        print("\nPrinting digits sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 1].values, n_sentences)
        print("\nPrinting cardinal sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 0].values, n_sentences)

    train_indices = train_caps_df.index.values
    test_indices = test_caps_df.index.values
    print("Fixing")
    for i, index_i in enumerate(train_indices):
        if pred_train[i] == 0:
            train["prediction"].iat[index_i] = word_parser.parse_letters(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 7
        elif pred_train[i] == 1:
            train["prediction"].iat[index_i] = word_parser.parse_plain(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 11
    for i, index_i in enumerate(test_indices):
        if pred_test[i] == 0:
            test["prediction"].iat[index_i] = word_parser.parse_letters(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 7
        elif pred_test[i] == 1:
            test["prediction"].iat[index_i] = word_parser.parse_plain(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 11

    return train, test

# [1099]	train-merror:0.008901	eval-merror:0.017362
# 1487
# [1009]	train-merror:0.008476	eval-merror:0.016271
