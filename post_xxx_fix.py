import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime

import word_parser
import plain_helpers
import dict_handler

import pyximport

pyximport.install()
import word_context


def fix_lowers(train: pd.DataFrame, test: pd.DataFrame, evaluation: pd.DataFrame, verbose=False):
    train_local_df = train[train["before"].map(lambda x: bool(re.fullmatch("[a-z][a-zé']*[ ,-]*", x)))]
    eval_local_df = evaluation[evaluation["before"].map(lambda x: bool(re.fullmatch("[a-z][a-zé']*[ ,-]*", x)))]
    test_local_df = test[test["before"].map(lambda x: bool(re.fullmatch("[a-z][a-zé']*[ ,-]*", x)))]

    def sum_errors(df):
        return np.sum(df["after"] != df["prediction"])

    print(sum_errors(train_local_df))
    if verbose:
        print(train_local_df["class_pred"].value_counts())

    train_local_df["hasS"] = train_local_df["before"].map(lambda x: int("'s" in x or x[-1] == "s"))
    eval_local_df["hasS"] = eval_local_df["before"].map(lambda x: int("'s" in x or x[-1] == "s"))
    test_local_df["hasS"] = test_local_df["before"].map(lambda x: int("'s" in x or x[-1] == "s"))

    train_local_df["hasPsik"] = train_local_df["before"].map(lambda x: int("'" in x))
    eval_local_df["hasPsik"] = eval_local_df["before"].map(lambda x: int("'" in x))
    test_local_df["hasPsik"] = test_local_df["before"].map(lambda x: int("'" in x))

    not_vowel = "([bcdfghjklmnpqrstvwxyz]|th|st|ch|sh|ght?|ss)"
    vowel = "([auioeé]|ea|ou|ee|ai|oa|ui|ei|ie|ue)"
    string_filter = not_vowel + "?" + "(" + vowel + not_vowel + "|i[oa]n)+[éey]?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_local_df["re_filter1a"] = train_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_local_df["re_filter1a"] = eval_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_local_df["re_filter1a"] = test_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    string_filter = not_vowel + "(" + vowel + not_vowel + "|i[oa]n)+[éey]?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_local_df["re_filter1b"] = train_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_local_df["re_filter1b"] = eval_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_local_df["re_filter1b"] = test_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    string_filter = not_vowel + "{1,2}" + "(" + vowel + not_vowel + "{1,2}" + "|i[oa]n)+([éeys]|ies)?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_local_df["re_filter2a"] = train_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_local_df["re_filter2a"] = eval_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_local_df["re_filter2a"] = test_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    string_filter = not_vowel + "{0,2}" + "(" + vowel + not_vowel + "{1,2}" + "|i[oa]n)+([éeys]|ies)?"
    re_filter = re.compile(string_filter, flags=re.IGNORECASE)
    train_local_df["re_filter2b"] = train_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    eval_local_df["re_filter2b"] = eval_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))
    test_local_df["re_filter2b"] = test_local_df["before"].map(lambda x: bool(re_filter.fullmatch(x)))

    train_local_df["letters_target"] = 0
    for i in range(train_local_df.shape[0]):
        if train_local_df["after"].iat[i] == word_parser.parse_letters(train_local_df["before"].iat[i]):
            train_local_df["letters_target"].iat[i] = 0
        elif train_local_df["after"].iat[i] == word_parser.parse_plain(train_local_df["before"].iat[i]):
            train_local_df["letters_target"].iat[i] = 1
        else:
            train_local_df["letters_target"].iat[i] = 2
    eval_local_df["letters_target"] = 0
    for i in range(eval_local_df.shape[0]):
        if eval_local_df["after"].iat[i] == word_parser.parse_letters(eval_local_df["before"].iat[i]):
            eval_local_df["letters_target"].iat[i] = 0
        elif eval_local_df["after"].iat[i] == word_parser.parse_plain(eval_local_df["before"].iat[i]):
            eval_local_df["letters_target"].iat[i] = 1
        else:
            eval_local_df["letters_target"].iat[i] = 2

    if verbose:
        print(train_local_df["letters_target"].value_counts())

    def print_sentences(sentences_id_arr, n):
        print("There are %d words" % sentences_id_arr.size)
        n = min(n, sentences_id_arr.size)
        for i in range(n):
            sentence = train[train["sentence_id"] == sentences_id_arr[i]]
            words = sentence["before"].values.tolist()
            words_after = sentence["after"].values.tolist()
            class_pred = sentence["class_pred"].values.astype(str).tolist()
            print(i)
            print(" ".join(words))
            print(" ".join(words_after))
            print(" ".join(class_pred))

    n_sentences = 10
    if verbose:
        print("\nPrinting 2 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_local_df["sentence_id"][train_local_df["letters_target"] == 2].values, n_sentences)
        print("\nPrinting 1 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_local_df["sentence_id"][train_local_df["letters_target"] == 1].values, n_sentences)
        print("\nPrinting 0 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_local_df["sentence_id"][train_local_df["letters_target"] == 0].values, n_sentences)

    def neigh_classes(full_df, df):
        df["before_class"] = -1
        df["before2_class"] = -1
        df["after_class"] = -1
        df["after2_class"] = -1
        indices = df.index.values
        tmp = full_df[["class_pred", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 2 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 2, 1]:
                    df["before2_class"].iat[i] = tmp[index_i - 2, 0]
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    df["before_class"].iat[i] = tmp[index_i - 1, 0]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    df["after_class"].iat[i] = tmp[index_i + 1, 0]
            if index_i + 2 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 2, 1]:
                    df["after2_class"].iat[i] = tmp[index_i + 2, 0]
        return df

    def neigh_number(full_df, df):
        df["before_number"] = -1
        df["after_number"] = -1
        indices = df.index.values
        tmp = full_df[["before", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i - 1, 0]):
                        if len(tmp[index_i - 1, 0]) < 8:
                            df["before_number"].iat[i] = int(tmp[index_i - 1, 0])
                        else:
                            df["before_number"].iat[i] = 999
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i + 1, 0]):
                        if len(tmp[index_i + 1, 0]) < 8:
                            df["after_number"].iat[i] = int(tmp[index_i + 1, 0])
                        else:
                            df["after_number"].iat[i] = 999
        return df

    def before_x(full_df, df):
        before_letters_series = pd.Series.from_csv("outputs/before_letters.csv")
        before_letters_index = before_letters_series.index.values
        after_letters_series = pd.Series.from_csv("outputs/after_letters.csv")
        after_letters_index = after_letters_series.index.values
        df["before_letters"] = 0
        df["after_letters"] = 0
        indices = df.index.values
        tmp = full_df["before"].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i - 1].lower() in before_letters_index:
                    df["before_letters"].iat[i] = before_letters_series[tmp[index_i - 1].lower()]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i + 1].lower() in after_letters_index:
                    df["after_letters"].iat[i] = after_letters_series[tmp[index_i + 1].lower()]
        return df

    train_local_df = neigh_classes(train, train_local_df)
    eval_local_df = neigh_classes(evaluation, eval_local_df)
    test_local_df = neigh_classes(test, test_local_df)

    train_local_df = before_x(train, train_local_df)
    eval_local_df = before_x(evaluation, eval_local_df)
    test_local_df = before_x(test, test_local_df)

    train_local_df = neigh_number(train, train_local_df)
    eval_local_df = neigh_number(evaluation, eval_local_df)
    test_local_df = neigh_number(test, test_local_df)

    train_local_df["length"] = train_local_df["before"].map(lambda x: len(x))
    eval_local_df["length"] = eval_local_df["before"].map(lambda x: len(x))
    test_local_df["length"] = test_local_df["before"].map(lambda x: len(x))

    train_local_df["n_ouaei"] = train_local_df["before"].map(lambda x: len(re.findall("[ouaei]", x)))
    eval_local_df["n_ouaei"] = eval_local_df["before"].map(lambda x: len(re.findall("[ouaei]", x)))
    test_local_df["n_ouaei"] = test_local_df["before"].map(lambda x: len(re.findall("[ouaei]", x)))

    train_local_df["n_non_ouaei"] = train_local_df["before"].map(
        lambda x: len(re.findall("[bcdfghjklmnpqrstvwxyz]", x)))
    eval_local_df["n_non_ouaei"] = eval_local_df["before"].map(
        lambda x: len(re.findall("[bcdfghjklmnpqrstvwxyz]", x)))
    test_local_df["n_non_ouaei"] = test_local_df["before"].map(
        lambda x: len(re.findall("[bcdfghjklmnpqrstvwxyz]", x)))

    train_local_df["percent_ouaei"] = train_local_df["n_ouaei"].values / train_local_df["length"].values
    eval_local_df["percent_ouaei"] = eval_local_df["n_ouaei"].values / eval_local_df["length"].values
    test_local_df["percent_ouaei"] = test_local_df["n_ouaei"].values / test_local_df["length"].values

    train_local_df["double_ouaei"] = train_local_df["before"].map(lambda x: len(re.findall("[ouaei]'[ouaei]", x)))
    eval_local_df["double_ouaei"] = eval_local_df["before"].map(lambda x: len(re.findall("[ouaei]'[ouaei]", x)))
    test_local_df["double_ouaei"] = test_local_df["before"].map(lambda x: len(re.findall("[ouaei]'[ouaei]", x)))

    print("Plain dict analysis ", datetime.now().strftime("%H:%M:%S"))
    plain_vc = dict_handler.create_vc_series("outputs/plain.csv", "[a-z][a-zé']*", 50000, 1000000000)
    train_local_df["plainDict"] = word_context.search_sorted_series(
        train_local_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    eval_local_df["plainDict"] = word_context.search_sorted_series(
        eval_local_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)
    test_local_df["plainDict"] = word_context.search_sorted_series(
        test_local_df["before"].values.tolist(),
        plain_vc.index.values.tolist(),
        plain_vc.values)

    print("Letters dict analysis ", datetime.now().strftime("%H:%M:%S"))
    letters_vc = dict_handler.create_vc_series("outputs/letters.csv", "[a-z][a-zé']*", 50000)
    train_local_df["lettersDict"] = word_context.search_sorted_series(
        train_local_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    eval_local_df["lettersDict"] = word_context.search_sorted_series(
        eval_local_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)
    test_local_df["lettersDict"] = word_context.search_sorted_series(
        test_local_df["before"].values.tolist(),
        letters_vc.index.values.tolist(),
        letters_vc.values)

    print("Verbatim dict analysis ", datetime.now().strftime("%H:%M:%S"))
    verbatim_vc = dict_handler.create_vc_series("outputs/verbatim.csv", "[a-z][a-zé']*", 50000)
    train_local_df["verbatimDict"] = word_context.search_sorted_series(
        train_local_df["before"].values.tolist(),
        verbatim_vc.index.values.tolist(),
        verbatim_vc.values)
    eval_local_df["verbatimDict"] = word_context.search_sorted_series(
        eval_local_df["before"].values.tolist(),
        verbatim_vc.index.values.tolist(),
        verbatim_vc.values)
    test_local_df["verbatimDict"] = word_context.search_sorted_series(
        test_local_df["before"].values.tolist(),
        verbatim_vc.index.values.tolist(),
        verbatim_vc.values)

    train_local_df["difference"] = train_local_df["plainDict"].values - train_local_df["lettersDict"].values
    eval_local_df["difference"] = eval_local_df["plainDict"].values - eval_local_df["lettersDict"].values
    test_local_df["difference"] = test_local_df["plainDict"].values - test_local_df["lettersDict"].values

    train_local_df["hasJunc"] = train_local_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1 and x != "x "))
    eval_local_df["hasJunc"] = eval_local_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1 and x != "x "))
    test_local_df["hasJunc"] = test_local_df["before"].map(lambda x: (
        x[-1] in [" ", ",", "-"] and len(x) > 1 and x != "x "))

    train_local_df["hasEEcute"] = train_local_df["before"].map(lambda x: "é" in x)
    eval_local_df["hasEEcute"] = eval_local_df["before"].map(lambda x: "é" in x)
    test_local_df["hasEEcute"] = test_local_df["before"].map(lambda x: "é" in x)

    xg_train = xgb.DMatrix(
        train_local_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class",
                        "length", "token_id", "n_ouaei", "percent_ouaei", "difference", "n_non_ouaei",
                        "hasJunc", "plainDict", "lettersDict", "verbatimDict", "hasEEcute", "double_ouaei",
                        "before_letters", "after_letters", "hasS", "hasPsik",
                        "re_filter1a", "re_filter1b", "re_filter2a", "re_filter2b"]].values,
        label=train_local_df["letters_target"].values)
    xg_eval = xgb.DMatrix(
        eval_local_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class",
                       "length", "token_id", "n_ouaei", "percent_ouaei", "difference", "n_non_ouaei",
                       "hasJunc", "plainDict", "lettersDict", "verbatimDict", "hasEEcute", "double_ouaei",
                       "before_letters", "after_letters", "hasS", "hasPsik",
                       "re_filter1a", "re_filter1b", "re_filter2a", "re_filter2b"]].values,
        label=eval_local_df["letters_target"].values)
    xg_test = xgb.DMatrix(
        test_local_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class",
                       "length", "token_id", "n_ouaei", "percent_ouaei", "difference", "n_non_ouaei",
                       "hasJunc", "plainDict", "lettersDict", "verbatimDict", "hasEEcute", "double_ouaei",
                       "before_letters", "after_letters", "hasS", "hasPsik",
                       "re_filter1a", "re_filter1b", "re_filter2a", "re_filter2b"]].values)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'multi:softmax'
    param['eval_metric'] = 'merror'
    # scale weight of positive examples
    param['eta'] = 0.03
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4
    param['num_class'] = 3

    watchlist = [(xg_train, 'train'), (xg_eval, 'eval')]
    num_round = 1200
    bst = xgb.train(param, xg_train, num_round, watchlist)

    # get prediction
    pred_train = bst.predict(xg_train)
    pred_test = bst.predict(xg_test)

    print(np.sum(pred_train != train_local_df["letters_target"]))

    if verbose:
        print(train_local_df[["before", "after", "prediction", "letters_target"]][pred_train !=
                                                                                  train_local_df["letters_target"]])

        wrong_prediction = train_local_df[pred_train != train_local_df["letters_target"]]
        print("\nPrinting date sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 2].values, n_sentences)
        print("\nPrinting digits sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 1].values, n_sentences)
        print("\nPrinting cardinal sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["letters_target"] == 0].values, n_sentences)

    train_indices = train_local_df.index.values
    test_indices = test_local_df.index.values
    print("Fixing")
    for i, index_i in enumerate(train_indices):
        if pred_train[i] == 0:
            train["prediction"].iat[index_i] = word_parser.parse_letters(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 7
        elif pred_train[i] == 1:
            train["prediction"].iat[index_i] = word_parser.parse_plain(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 11
    for i, index_i in enumerate(test_indices):
        if pred_test[i] == 0:
            test["prediction"].iat[index_i] = word_parser.parse_letters(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 7
        elif pred_test[i] == 1:
            test["prediction"].iat[index_i] = word_parser.parse_plain(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 11

    return train, test
