from glob import glob
import csv
import pandas as pd

file_list = sorted(glob("inputs/en_with_types/*"))
last_row = ["", "", ""]
words_before_ordinal = []
words_before_date = []
words_before_cardinal = []
words_before_letters = []
words_after_letters = []
words_after_cardinals = []
words_after_ordinals = []
words_after_date = []

words_before_digit = []
words_after_digit = []

flag_letter = False
flag_cardinal = False
flag_ordinal = False
flag_date = False
flag_digit = False
for file in file_list:
    print(file)
    with open(file) as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        for row in reader:
            if len(row) == 3:
                if flag_letter:
                    words_after_letters.append(row[1].lower())
                    flag_letter = False
                if flag_cardinal:
                    words_after_cardinals.append(row[1].lower())
                    flag_cardinal = False
                if flag_date:
                    words_after_date.append(row[1].lower())
                    flag_date = False
                if flag_ordinal:
                    words_after_ordinals.append(row[1].lower())
                    flag_ordinal = False
                if flag_digit:
                    words_after_digit.append(row[1].lower())
                    flag_digit = False
                if row[0] == "CARDINAL":
                    words_before_cardinal.append(last_row[1].lower())
                    flag_cardinal = True
                if row[0] == "ORDINAL":
                    words_before_ordinal.append(last_row[1].lower())
                    flag_ordinal = True
                if row[0] == "LETTERS":
                    words_before_letters.append(last_row[1].lower())
                    flag_letter = True
                if row[0] == "DATE":
                    words_before_date.append(last_row[1].lower())
                    flag_date = True
                if row[0] == "DIGIT":
                    words_before_digit.append(last_row[1].lower())
                    flag_digit = True
                last_row = row

# print("cardinal")
# print(pd.Series(words_before_cardinal).value_counts().iloc[:1000])
# pd.Series(words_before_cardinal).value_counts().iloc[:1000].to_csv("outputs/before_cardinals.csv")
# print("ordinal")
# print(pd.Series(words_before_ordinal).value_counts().iloc[:1000])
# pd.Series(words_before_ordinal).value_counts().iloc[:1000].to_csv("outputs/before_ordinals.csv")
# print("letters before")
# print(pd.Series(words_before_letters).value_counts().iloc[:1000])
# pd.Series(words_before_letters).value_counts().iloc[:1000].to_csv("outputs/before_letters.csv")
# print("letters after")
# print(pd.Series(words_after_letters).value_counts().iloc[:1000])
# pd.Series(words_after_letters).value_counts().iloc[:1000].to_csv("outputs/after_letters.csv")
# print("cardinals after")
# print(pd.Series(words_after_cardinals).value_counts().iloc[:1000])
# pd.Series(words_after_cardinals).value_counts().iloc[:1000].to_csv("outputs/after_cardinals.csv")
# print("ordinals after")
# print(pd.Series(words_after_ordinals).value_counts().iloc[:1000])
# pd.Series(words_after_ordinals).value_counts().iloc[:1000].to_csv("outputs/after_ordinals.csv")
#
# print("date before")
# print(pd.Series(words_before_date).value_counts().iloc[:1000])
# pd.Series(words_before_date).value_counts().iloc[:1000].to_csv("outputs/before_date.csv")
# print("date after")
# print(pd.Series(words_after_date).value_counts().iloc[:1000])
# pd.Series(words_after_date).value_counts().iloc[:1000].to_csv("outputs/after_date.csv")


print("date before")
print(pd.Series(words_before_digit).value_counts().iloc[:1000])
pd.Series(words_before_digit).value_counts().iloc[:1000].to_csv("outputs/before_digit.csv")
print("date after")
print(pd.Series(words_after_digit).value_counts().iloc[:1000])
pd.Series(words_after_digit).value_counts().iloc[:1000].to_csv("outputs/after_digit.csv")
