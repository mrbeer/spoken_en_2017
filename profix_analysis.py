import pandas as pd
import numpy as np
import datetime

script_version = "4.0"
post_version = "18"
print("Reading files ", datetime.datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("outputs/train.{0}.csv".format(script_version))
train["before"] = train["before"].astype(str)
train["prediction"] = train["prediction"].astype(str)
train["after"] = train["after"].astype(str)
train["class_pred"] = train["class_pred"].astype(float).astype(int)

unsolved = pd.DataFrame.from_csv("outputs/not_solved_{0}.csv".format(post_version))

print(unsolved["before"].value_counts())
print(unsolved["class"].value_counts())

for value in unsolved["before"].value_counts().iloc[:40].index.values:
    print(value)
    print(train["after"][train["before"] == value].value_counts())

# sr -> (senior, s r)
# SR -> (senior, s r)
# st -> (saint, street)
# Québec -> quebec
# PLoS -> PLoS
# advertisements? -> <self>
# television -> <self>
