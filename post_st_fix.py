import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime


def fix_st(train: pd.DataFrame, test: pd.DataFrame):
    train_to_df = train[train["before"].map(lambda x: bool(re.fullmatch("st", x)))]
    test_to_df = test[test["before"].map(lambda x: bool(re.fullmatch("st", x)))]

    def sum_errors(df):
        return np.sum(df["after"] != df["prediction"])

    print(sum_errors(train_to_df))
    # print(train_to_df["after"].value_counts())

    train_to_df["street"] = (train_to_df["after"] == "street").astype(int)

    def print_sentences(sentences_id_arr, n):
        print("There are %d sentences" % sentences_id_arr.size)
        n = min(n, sentences_id_arr.size)
        for i in range(n):
            sentence = train[train["sentence_id"] == sentences_id_arr[i]]
            words = sentence["before"].values.tolist()
            words_after = sentence["after"].values.tolist()
            class_pred = sentence["class_pred"].values.astype(str).tolist()
            print(i)
            print(" ".join(words))
            print(" ".join(words_after))
            print(" ".join(class_pred))

    # n_sentences = 10
    # print("\nPrinting transformed sentences", datetime.now().strftime("%H:%M:%S"))
    # print_sentences(train_to_df["sentence_id"][train_to_df["street"] == 1].values, n_sentences)
    # print("\nPrinting not transformed sentences", datetime.now().strftime("%H:%M:%S"))
    # print_sentences(train_to_df["sentence_id"][train_to_df["street"] == 0].values, n_sentences)

    def neigh_classes(full_df, df):
        df["before_class"] = -1
        df["before2_class"] = -1
        df["after_class"] = -1
        df["after2_class"] = -1
        indices = df.index.values
        tmp = full_df[["class_pred", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 2 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 2, 1]:
                    df["before2_class"].iat[i] = tmp[index_i - 2, 0]
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    df["before_class"].iat[i] = tmp[index_i - 1, 0]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    df["after_class"].iat[i] = tmp[index_i + 1, 0]
            if index_i + 2 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 2, 1]:
                    df["after2_class"].iat[i] = tmp[index_i + 2, 0]
        return df

    def neigh_number(full_df, df):
        df["before_number"] = -1
        df["after_number"] = -1
        indices = df.index.values
        tmp = full_df[["before", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i - 1, 0]):
                        df["before_number"].iat[i] = int(tmp[index_i - 1, 0])
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i + 1, 0]):
                        df["after_number"].iat[i] = int(tmp[index_i + 1, 0])
        return df

    train_to_df = neigh_classes(train, train_to_df)
    test_to_df = neigh_classes(test, test_to_df)

    train_to_df = neigh_number(train, train_to_df)
    test_to_df = neigh_number(test, test_to_df)

    train_to_df["int_diff"] = train_to_df["after_number"].values - train_to_df["before_number"].values
    test_to_df["int_diff"] = test_to_df["after_number"].values - test_to_df["before_number"].values

    train_to_df["is_colon"] = (train_to_df["before"] == ":").astype(int)
    test_to_df["is_colon"] = (test_to_df["before"] == ":").astype(int)

    xg_train = xgb.DMatrix(
        train_to_df[["before_class", "before2_class", "after_class", "after2_class", "int_diff", "is_colon",
                     "class_pred"]].values,
        label=train_to_df["street"].values)
    xg_test = xgb.DMatrix(
        test_to_df[["before_class", "before2_class", "after_class", "after2_class", "int_diff", "is_colon",
                    "class_pred"]].values)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'binary:logistic'
    param['eval_metric'] = 'error'
    # scale weight of positive examples
    param['eta'] = 0.1
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4

    watchlist = [(xg_train, 'train')]
    num_round = 200
    bst = xgb.train(param, xg_train, num_round, watchlist)

    # get prediction
    pred_train = bst.predict(xg_train)
    pred_train = (pred_train > 0.5).astype(int)
    pred_test = bst.predict(xg_test)
    pred_test = (pred_test > 0.5).astype(int)

    train_to_df["street_pred"] = pred_train
    print(np.sum(train_to_df["street_pred"] != train_to_df["street"]))
    # print(train_to_df[["before", "after", "prediction", "street_pred"]][train_to_df["after"] !=
    #                                                                    train_to_df["prediction"]])

    # wrong_prediction = train_to_df[train_to_df["street_pred"] != train_to_df["street"]]
    # print("\nPrinting transformed sentences", datetime.now().strftime("%H:%M:%S"))
    # print_sentences(wrong_prediction["sentence_id"][wrong_prediction["street"] == 1].values, n_sentences)
    # print("\nPrinting not transformed sentences", datetime.now().strftime("%H:%M:%S"))
    # print_sentences(wrong_prediction["sentence_id"][wrong_prediction["street"] == 0].values, n_sentences)

    print("Fixing")
    train_indices = train_to_df.index.values
    for i, index_i in enumerate(train_indices):
        if pred_train[i]:
            train["prediction"].iat[index_i] = "street"
        else:
            train["prediction"].iat[index_i] = "saint"
    test_indices = test_to_df.index.values
    for i, index_i in enumerate(test_indices):
        if pred_test[i]:
            test["prediction"].iat[index_i] = "street"
        else:
            test["prediction"].iat[index_i] = "saint"
    return train, test
