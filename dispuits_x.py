from datetime import datetime
import pandas as pd
import numpy as np
import re

print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("inputs/en_train.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)

after_word = "by"
after_word_df = train[train["after"] == after_word]
after_word_df = after_word_df[after_word_df["before"] != after_word]
print(after_word_df["before"].values.tolist())

before_regex = re.compile("x")
df = train[train["before"].map(lambda x: bool(before_regex.fullmatch(x)))]
df["transform"] = (df["after"] == after_word).astype(int)

df_true = df[df["transform"] == 1]
df_false = df[df["transform"] == 0]
n_sentences = 40


def print_sentences(train, sentences_id_arr, n):
    n = min(n, sentences_id_arr.size)
    for i in range(n):
        sentence = train[train["sentence_id"] == sentences_id_arr[i]]
        words = sentence["before"].values.tolist()
        print(" ".join(words))


print("\nPrinting transformed sentences", datetime.now().strftime("%H:%M:%S"))
print_sentences(train, df_true["sentence_id"].values, n_sentences)
print("\nPrinting not transformed sentences", datetime.now().strftime("%H:%M:%S"))
print_sentences(train, df_false["sentence_id"].values, n_sentences)
