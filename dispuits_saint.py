from datetime import datetime
import pandas as pd
import numpy as np
import re

print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("inputs/en_train.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)

after_word = "street"
after_word_df = train[train["after"] == after_word]
after_word_df = after_word_df[after_word_df["before"] != after_word]
print(after_word_df["before"].values.tolist())

before_regex = re.compile("[Ss][Tt]")
df = train[train["before"].map(lambda x: bool(before_regex.fullmatch(x)))]
df["transform"] = (df["after"] == after_word).astype(int)

df_true = df[df["transform"] == 1]
df_false = df[df["transform"] == 0]
n_sentences = 20


def print_sentences(train, sentences_id_arr, n):
    n = min(n, sentences_id_arr.size)
    for i in range(n):
        sentence = train[train["sentence_id"] == sentences_id_arr[i]]
        words = sentence["before"].values.tolist()
        print(" ".join(words))


print("\nPrinting transformed sentences", datetime.now().strftime("%H:%M:%S"))
print_sentences(train, df_true["sentence_id"].values, n_sentences)
print("\nPrinting not transformed sentences", datetime.now().strftime("%H:%M:%S"))
print_sentences(train, df_false["sentence_id"].values, n_sentences)

name_re = re.compile("[A-Z][a-z]+'?s?")


def name_after_word(index_arr):
    answers = []
    for i in range(index_arr.size):
        index_i = index_arr[i]
        word = train["before"].iat[index_i+1]
        sentence_id = train["sentence_id"].iat[index_i]
        sentence = " ".join(train["before"][train["sentence_id"] == sentence_id].values.tolist())
        if not bool(name_re.fullmatch(word)):
            answers.append(1)
        else:
            answers.append(0)
    return answers


df_true["name_after"] = name_after_word(df_true.index.values)
df_false["name_after"] = name_after_word(df_false.index.values)

print(df_true.head(n=10))
print(df_false.head(n=10))

print(np.mean(df_true["name_after"].values))
print(np.mean(df_false["name_after"].values))

print(np.sum(df_true["name_after"].values))
print(np.sum(df_false["name_after"].values))
