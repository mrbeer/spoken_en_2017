import pandas as pd
import numpy as np
import re
from datetime import datetime

from post_to_fix import fix_to
from post_bool_fix import fix_bool
from post_num_fix import fix_nums
from post_roman_fix import fix_romans
from post_XXX_fix import fix_caps
from post_Xxxx_fix import fix_names
from post_xXxx_fix import fix_caps_and_lowers
from post_xxx_fix import fix_lowers
import word_parser

script_version = "4.3"
post_version = "19"
post_post_version = "1"

print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("outputs/train.{0}.csv".format(script_version))
train["before"] = train["before"].astype(str)
train["prediction"] = train["prediction"].astype(str)
train["after"] = train["after"].astype(str)
train["class_pred"] = train["class_pred"].astype(float).astype(int)

unsolved = pd.DataFrame.from_csv("outputs/not_solved_{0}.csv".format(post_version))
unsolved["before"] = unsolved["before"].astype(str)
unsolved = unsolved[unsolved["class"].map(lambda x: x in ["PLAIN", "LETTERS"])]
print(unsolved)

test = pd.read_csv("outputs/test.{0}.csv".format(script_version), dtype=str)
test["before"] = test["before"].astype(str)
test["prediction"] = test["prediction"].astype(str)
test["class_pred"] = test["class_pred"].astype(float).astype(int)

submission = pd.DataFrame.from_csv("outputs/v{0}.{1}.csv".format(script_version, post_version))
submission["after"] = test["prediction"].values

unsolved_before_unique = np.unique(unsolved["before"].values)
test_before_unique = np.unique(test["before"].values)
number_fixes = 0
for i, value in enumerate(unsolved_before_unique):
    if value in test_before_unique:
        print(i, value)
        local_train = train[train["before"] == value]
        train_after_choices = np.unique(local_train["after"].values)
        print(train_after_choices)
        if train_after_choices.size == 1:
            local_test = test[test["before"] == value]
            print("fix test")
            local_test_index = local_test.index.values
            for test_index_i in local_test_index:
                before_fix = submission["after"].iloc[test_index_i]
                print(submission["after"].iloc[test_index_i])
                submission["after"].iat[test_index_i] = train_after_choices[0]
                after_fix = submission["after"].iloc[test_index_i]
                print("\t", after_fix)
                if before_fix != after_fix:
                    number_fixes += 1
                    print("xxxfixedxxx")

print(number_fixes)
print("Saving submit ", datetime.now().strftime("%H:%M:%S"))

submission.to_csv("outputs/v{0}.{1}.{2}.csv".format(script_version, post_version, post_post_version), encoding="UTF-8")
print("Done ", datetime.now().strftime("%H:%M:%S"))

# sr -> (senior, s r)
# SR -> (senior, s r)
# st -> (saint, street)
# Québec -> quebec
# PLoS -> PLoS
# advertisements -> <self>
# advertisement -> <self>
# television -> <self>
