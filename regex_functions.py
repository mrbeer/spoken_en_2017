import re


def string_to_cardinal(string: str):
    minus = False
    if string[0] == "-":
        minus = True
    string = re.split("[, -]", string)
    string = "".join(string)
    if len(string) < 8:
        if minus:
            string = "-" + string
        try:
            integer = int(string)
            return integer
        except:
            return -1
    else:
        return -1
