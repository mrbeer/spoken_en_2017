from nltk.stem.snowball import SnowballStemmer
import pandas as pd

stemmer = SnowballStemmer("english")

if __name__ == '__main__':
    train = pd.read_csv("inputs/en_train.csv")
    train["before"] = train["before"].astype(str)
    train["after"] = train["after"].astype(str)
    train["stem"] = train["before"].map(lambda x: stemmer.stem(x))
    train["stem_equal"] = (train["before"].values == train["stem"].values).astype(int)
    print(train)
    print(train["class"].value_counts())
    print(train["class"][train["stem_equal"] == 0].value_counts())
