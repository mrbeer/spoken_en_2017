import pandas as pd
import numpy as np
import re
import xgboost as xgb
from datetime import datetime

from sklearn import preprocessing

import word_parser
import verbatim_lib
import plain_helpers
import dict_handler

from word_parser import build_plain_dictionaries
from plain_helpers import is_capital_stuck, auoei_ratio
from re_filters import full_match, count_regex
from word_counting import sentence_word_counter_dict, sentence_word_counter, token_normaliser, \
    sentence_alpha_word_counter_dict
from regex_functions import string_to_cardinal

SESSION = "live"

print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("inputs/en_train_amended.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)

# train = train.iloc[:100000]

test = pd.read_csv("inputs/en_test_2.csv", dtype=str)
test["before"] = test["before"].astype(str)

print("Digit features ", datetime.now().strftime("%H:%M:%S"))
train["firstZero"] = train["before"].map(lambda x: x[0] == "0").astype(int)
test["firstZero"] = test["before"].map(lambda x: x[0] == "0").astype(int)

train["cardinalValue"] = train["before"].map(lambda x: string_to_cardinal(x)).astype(int)
test["cardinalValue"] = test["before"].map(lambda x: string_to_cardinal(x)).astype(int)

print("measure affix check ", datetime.now().strftime("%H:%M:%S"))

measure_number_string = "0123456789-,. "
SUP = str.maketrans("⁰¹²³⁴⁵⁶⁷⁸⁹", "0123456789")


def get_unit_vc(string: str):
    string = string.translate(SUP)
    unit = ""
    for i, char in enumerate(string):
        if char not in measure_number_string:
            if char == "/" and i + 1 < len(string) and string[i + 1] in "0123456789":
                i += 1
                while i < len(string) and string[i] in "0123456789":
                    i += 1
                unit = string[i:].replace(" ", "")
                break
            else:
                unit = string[i:].replace(" ", "")
    if unit in word_parser.measure_unit_df.index.values:
        return word_parser.measure_unit_df["vc"].at[unit]
    else:
        return 0


train["unit_vc"] = train["before"].map(lambda x: get_unit_vc(x))
test["unit_vc"] = test["before"].map(lambda x: get_unit_vc(x))

print("AUOEI letters ratio ", datetime.now().strftime("%H:%M:%S"))
train["auoeiRatio"] = train["before"].map(lambda x: auoei_ratio(x))
test["auoeiRatio"] = test["before"].map(lambda x: auoei_ratio(x))

print("Plain dict analysis ", datetime.now().strftime("%H:%M:%S"))
plain_vc = dict_handler.create_vc_series("outputs/plain.csv", ".*", 5000, 10000000)
train["plainDict"] = train["before"].map(
    lambda x: dict_handler.search_sorted_series(x, plain_vc))
test["plainDict"] = test["before"].map(
    lambda x: dict_handler.search_sorted_series(x, plain_vc))

print("Letters dict analysis ", datetime.now().strftime("%H:%M:%S"))
letters_vc = dict_handler.create_vc_series("outputs/letters.csv", ".*", 5000)
train["lettersDict"] = train["before"].map(
    lambda x: dict_handler.search_sorted_series(x, letters_vc))
test["lettersDict"] = test["before"].map(
    lambda x: dict_handler.search_sorted_series(x, letters_vc))

print("Verbatim dict analysis ", datetime.now().strftime("%H:%M:%S"))
verbatim_vc = dict_handler.create_vc_series("outputs/verbatim.csv", ".*", 5000)
train["verbatimDict"] = train["before"].map(
    lambda x: dict_handler.search_sorted_series(x, verbatim_vc))
test["verbatimDict"] = test["before"].map(
    lambda x: dict_handler.search_sorted_series(x, verbatim_vc))

train["plainLettersDif"] = train["plainDict"].values - train["lettersDict"].values - train["verbatimDict"].values
test["plainLettersDif"] = test["plainDict"].values - test["lettersDict"].values - test["verbatimDict"].values

print("Check for caps sentences ", datetime.now().strftime("%H:%M:%S"))
train = is_capital_stuck(train)
test = is_capital_stuck(test)

print("Building Plain transformations dict ", datetime.now().strftime("%H:%M:%S"))
build_plain_dictionaries(1000000000)

print("Counting words ", datetime.now().strftime("%H:%M:%S"))
train_sentence_word_count_dict = sentence_word_counter_dict(train)
test_sentence_word_count_dict = sentence_word_counter_dict(test)

train["sentence_n_words"] = sentence_word_counter(train[["sentence_id"]], train_sentence_word_count_dict)
test["sentence_n_words"] = sentence_word_counter(test[["sentence_id"]], test_sentence_word_count_dict)

print("Normalising tokens ", datetime.now().strftime("%H:%M:%S"))
train["normalised_tokens"] = token_normaliser(train[["token_id", "sentence_n_words"]])
test["normalised_tokens"] = token_normaliser(test[["token_id", "sentence_n_words"]])

print("Counting alpha words ", datetime.now().strftime("%H:%M:%S"))
train_sentence_alpha_word_count_dict = sentence_alpha_word_counter_dict(train)
test_sentence_alpha_word_count_dict = sentence_alpha_word_counter_dict(test)

train["sentenceAlphawords"] = sentence_word_counter(train[["sentence_id"]], train_sentence_word_count_dict)
test["sentenceAlphawords"] = sentence_word_counter(test[["sentence_id"]], test_sentence_word_count_dict)

print("Basic parameters ", datetime.now().strftime("%H:%M:%S"))
train["nDigits"], test["nDigits"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[0-9]"))

train["nCapitals"], test["nCapitals"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[A-Z]"))

train["nSmalls"], test["nSmalls"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[a-z]"))

train["nLetters"], test["nLetters"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[a-zA-Z]"))

train["isName"], test["isName"] = full_match(
    train[["before", "class"]], test[["before"]], re.compile("[A-Z][a-z]+'?s?"), "PLAIN")

train["length"] = train["before"].map(lambda x: len(x))
test["length"] = test["before"].map(lambda x: len(x))

print("Basic split parameters ", datetime.now().strftime("%H:%M:%S"))
print("address")
address_pattern = re.compile("(Interstate|U\.?S\.?|SR|CR|[A-Z])[ -]?[0-9]+,? ?")
train["isAddress"], test["isAddress"] = full_match(
    train[["before", "class"]], test[["before"]], address_pattern, "ADDRESS")

print("cardinals")
cardinal_pattern = re.compile("(-[1-9]|-?0|-?[0-9,]{2,}|[IVXLCDM]+\.?'?s?|[0-9]{1,3}(,? [0-9]{3})+)" +
                              " ?[:-]? ?(A|M|[Uu]\.?[Ss]\.? )?")
train["isCardinal"], test["isCardinal"] = full_match(
    train[["before", "class"]], test[["before"]], cardinal_pattern, "CARDINAL")

print("date")
weekdays_re = "((Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday),? )"
months_re = "(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|June?|" + \
            "July?|Aug(ust)?|Sept?(ember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\.?,?"
months_num_dict = {1: "january", 2: "february", 3: "march", 4: "april",
                   5: "may", 6: "june", 7: "july", 8: "august", 9: "september",
                   10: "october", 11: "november", 12: "december"}

date_pattern = re.compile("(the )?" + weekdays_re + "?"
                                                    "(" +
                          "[0-9]{2}'?s|[0-9]{3,4}('?s)?,? ?|" +
                          "[0-9]{1,4}[/.][0-9]{1,2}[/.][0-9]{1,4}|[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,4}|" +
                          "[0-9]{1,4}-" + months_re + "-[0-9]{1,4}|" +
                          "([0-9]{1,2}(th|nd|st|rd)? )?" + months_re + " [0-9]{2,4}|" +
                          months_re + " [0-9]{1,2}(th|nd|st|rd)?,? [0-9]{2,4}|" +
                          months_re + " [0-9]{1,2}(th|nd|st|rd)?|" +
                          "[0-9]{1,2}(th|nd|st|rd)?,? " + months_re + "|" +
                          "[0-9]{1,6} ?(B\.?C\.?|A\.?[DF]\.?|(B\.?)?C\.?E\.?)" +
                          ")" +
                          "/?,? ?", flags=re.IGNORECASE)

train["isDate"], test["isDate"] = full_match(
    train[["before", "class"]], test[["before"]], date_pattern, "DATE")

print("decimal")
decimal_pattern = re.compile("-?[0-9]{,3}(,?[0-9]{3})*\.[0-9]+( million| billion| thousand| trillion)?|" +
                             "[0-9]{,3}(,?[0-9]{3})*( million| billion| thousand| trillion)")
train["isDecimal"], test["isDecimal"] = full_match(
    train[["before", "class"]], test[["before"]], decimal_pattern, "DECIMAL")

print("digit")
digits_pattern = re.compile("([0-9] )*[0-9]+-?,? ?")
train["isDigits"], test["isDigits"] = full_match(
    train[["before", "class"]], test[["before"]], digits_pattern, "DIGIT")

print("electronic")
electronic_pattern = re.compile(
    "#[A-Za-z0-9.-]+|::|.*\.[a-z]{2,4}(/.*|[A-Z][a-z]*|[0-9]+)?|.*https?://.*|[Ww]{3}\..*|" +
    "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}|\.[0-9][.-][0-9A-Za-z]+")
train["isElectronic"], test["isElectronic"] = full_match(
    train[["before", "class"]], test[["before"]], electronic_pattern, "ELECTRONIC")

print("fraction")
fraction_pattern = re.compile("-? ?[0-9]* ?[0-9,]+ ?/ ?[0-9,]+|.*[½⅓⅔¼¾⅛⅝⅞].*")
train["isFraction"], test["isFraction"] = full_match(
    train[["before", "class"]], test[["before"]], fraction_pattern, "FRACTION")

print("letters")
letters_pattern = re.compile("[BCDFGHJKLMNPQRSTVWXYZ]?[bcdfghjklmnpqrstvwxyz]+|" +
                             "([A-Z][.&]? ?)+'?[s.-]? ?|[a-zé]|[A-Z]?[a-z]+[A-Z]+|([a-z]\.)+")
train["isLetters"], test["isLetters"] = full_match(
    train[["before", "class"]], test[["before"]], letters_pattern, "LETTERS")

print("measure")
measure_pattern = re.compile("-?[0-9]+(,[0-9]{3})*(\.[0-9]+)? ?(sq )?([μa-zA-Z]{1,3}[23²³]?|%|'|\"|percent)|" +
                             "-?[0-9]+(,[0-9]{3})*(\.[0-9]+)? ?(sq )?[μa-zA-Z]{,3}[23²³]?/[a-zA-Z]{1,3}[23²³]?|" +
                             "(/|per) ?[a-zA-Z]+[23²³]?")
train["isMeasure"], test["isMeasure"] = full_match(
    train[["before", "class"]], test[["before"]], measure_pattern, "MEASURE")

print("money")
money_pattern = re.compile("((US)?A?(NZ)?\$|£|€|¥)[0-9]+(,[0-9]{3})*(.[0-9]{1,3})?" +
                           " ?([kK]|[Tt](rillion)?|[Mm](illion)?|[Bb]n?|[Bb](illion)?)?")
train["isMoney"], test["isMoney"] = full_match(
    train[["before", "class"]], test[["before"]], money_pattern, "MONEY")

print("ordinal")
ordinal_pattern = re.compile("([0-9, ]*(1 ?[Ss][Tt]|2 ?[Nn][Dd]|3 ?[Rr][Dd]|[0-9] ?[Tt][Hh]) ?|" +
                             "[IVXLCDM]+(st|nd|th|\.)?)('?s)?|[0-9, ]*[0-9][ºª]")
train["isOrdinal"], test["isOrdinal"] = full_match(
    train[["before", "class"]], test[["before"]], ordinal_pattern, "ORDINAL")

print("telephone")
phone_pattern = re.compile("[0-9]+|(\(?[0-9]+[) x-]+)+[0-9]+|(\(?[0-9]+[) x-]+)+[A-Z]+")
train["isPhone"], test["isPhone"] = full_match(
    train[["before", "class"]], test[["before"]], phone_pattern, "TELEPHONE")

print("plain")
plain_pattern = re.compile("[A-Z:-]|([DdOoLl]'|La|Le)?[A-Z]?[a-zé]+('s|s'|'t|'re|'ve|'l)?|" +
                           "I'(ve|m)|[A-Z]?[a-z]+[A-Z][a-z]+")
train["isPlain"], test["isPlain"] = full_match(
    train[["before", "class"]], test[["before"]], plain_pattern, "PLAIN")

print("punct")
punct_pattern = re.compile("[.,()—:\"'/¡!?;«»¿\[\]|]|-+")
train["isPunct"], test["isPunct"] = full_match(
    train[["before", "class"]], test[["before"]], punct_pattern, "PUNCT")

print("time")
time_pattern = re.compile(
    "([0-9]{1,2} ?[:.] ?[0-9]{2}( ?[:.] ?[0-9]{1,2})? ?(A\.?M.?|P\.?M.?|a\.?m\.?|p\.?m\.?)?|" +
    "[0-9]{1,2} ?(A\.?M\.?|P\.?M\.?|a\.?m\.?|p\.?m\.?)) ?[A-Z]*")
train["isTime"], test["isTime"] = full_match(
    train[["before", "class"]], test[["before"]], time_pattern, "TIME")

print("verbatim")
verbatim_pattern = re.compile(".|C#")


train["isVerbatim"], test["isVerbatim"] = full_match(
    train[["before", "class"]], test[["before"]], verbatim_pattern, "VERBATIM")

verbatim_char_dict = verbatim_lib.build_verbatim_char_dict(train[["before", "after", "class"]])

pd.Series(np.sum(
    test[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
          "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
          "isVerbatim"]].values, axis=1)).value_counts()

print("Label encoding ", datetime.now().strftime("%H:%M:%S"))
le = preprocessing.LabelEncoder()
le.fit(train["class"].values)
train["class_i"] = le.transform(train["class"].values)

print("First class prediction ", datetime.now().strftime("%H:%M:%S"))


def predict_train_test_classes(train, train_y, test, n):
    xg_train = xgb.DMatrix(train, label=train_y)
    xg_test = xgb.DMatrix(test)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'multi:softmax'
    param['eval_metric'] = 'merror'
    # scale weight of positive examples
    param['eta'] = 0.1
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4
    param['num_class'] = 16

    watchlist = [(xg_train, 'train')]
    num_round = n
    bst = xgb.train(param, xg_train, num_round, watchlist)
    # get prediction
    pred_train = bst.predict(xg_train)
    pred_test = bst.predict(xg_test)

    return pred_train, pred_test


train["class_pred"], test["class_pred"] = predict_train_test_classes(
    train[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
           "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime", "token_id",
           "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens", "isName",
           "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio", "verbatimDict",
           "plainLettersDif", "firstZero", "cardinalValue", "isVerbatim", "unit_vc"]].values,
    train["class_i"].values,
    test[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
          "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime", "token_id",
          "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens", "isName",
          "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio", "verbatimDict",
          "plainLettersDif", "firstZero", "cardinalValue", "isVerbatim", "unit_vc"]].values, 120)


def neigh_inference(df):
    df["before1_class_iter"] = -1
    df["before2_class_iter"] = -1
    df["after1_class_iter"] = -1
    df["after2_class_iter"] = -1
    tmp = df[["class_pred", "sentence_id"]].values
    for i in np.arange(tmp.shape[0]):
        if i - 1 >= 0:
            if tmp[i, 1] == tmp[i - 1, 1]:
                df["before1_class_iter"].iat[i] = tmp[i - 1, 0]
        if i - 2 >= 0:
            if tmp[i, 1] == tmp[i - 2, 1]:
                df["before2_class_iter"].iat[i] = tmp[i - 2, 0]
        if i + 2 < tmp.shape[0]:
            if tmp[i, 1] == tmp[i + 2, 1]:
                df["after2_class_iter"].iat[i] = tmp[i + 2, 0]
        if i + 1 < tmp.shape[0]:
            if tmp[i, 1] == tmp[i + 1, 1]:
                df["after1_class_iter"].iat[i] = tmp[i + 1, 0]
    return df


context_iterations = 3
for i in range(context_iterations):
    print("Iteration %d, finding context " % (i + 1), datetime.now().strftime("%H:%M:%S"))
    train = neigh_inference(train)
    test = neigh_inference(test)
    print("Class prediction with context ", datetime.now().strftime("%H:%M:%S"))

    train["class_pred"], test["class_pred"] = predict_train_test_classes(
        train[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
               "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
               "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens",
               "before1_class_iter", "before2_class_iter", "after1_class_iter", "after2_class_iter", "token_id",
               "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio", "verbatimDict",
               "plainLettersDif", "firstZero", "cardinalValue", "isVerbatim", "isName", "unit_vc",
               "class_pred"]].values,
        train["class_i"].values,
        test[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
              "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
              "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens",
              "before1_class_iter", "before2_class_iter", "after1_class_iter", "after2_class_iter", "token_id",
              "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio", "verbatimDict",
              "plainLettersDif", "firstZero", "cardinalValue", "isVerbatim", "isName", "unit_vc",
              "class_pred"]].values, 150)

print(train["class_i"].value_counts())
print(train["class_pred"].value_counts())
print(test["class_pred"].value_counts())
print(list(zip(range(16), le.classes_)))

print("Predicting words ", datetime.now().strftime("%H:%M:%S"))


def predict_word(string, str_class):
    if str_class == 0:
        return word_parser.parse_address(string)
    elif str_class == 1:
        return word_parser.parse_cardinal(string)
    elif str_class == 2:
        return word_parser.parse_date(string)
    elif str_class == 3:
        return word_parser.parse_decimal(string)
    elif str_class == 4:
        return word_parser.parse_digits(string)
    elif str_class == 5:
        return word_parser.parse_electronic(string)
    elif str_class == 6:
        return word_parser.parse_fraction(string)
    elif str_class == 7:
        return word_parser.parse_letters(string)
    elif str_class == 8:
        return word_parser.parse_measure(string)
    elif str_class == 9:
        return word_parser.parse_money(string)
    elif str_class == 10:
        return word_parser.parse_ordinals(string)
    elif str_class == 11:
        return word_parser.parse_plain(string)
    elif str_class == 12:
        return word_parser.parse_punct(string)
    elif str_class == 13:
        return word_parser.parse_phone(string)
    elif str_class == 14:
        return word_parser.parse_time(string)
    elif str_class == 15:
        return word_parser.parse_verbatim(string, verbatim_char_dict)


def predict_df(df):
    df["prediction"] = ""
    for i, before in enumerate(df["before"].values):
        df["prediction"].iat[i] = predict_word(before, df["class_pred"].iat[i])
    return df


test = predict_df(test)

submission = pd.DataFrame.from_csv("inputs/en_sample_submission_2.csv")
submission["after"] = test["prediction"].values
submission.to_csv("outputs/v4.3.csv", encoding="UTF-8")

print("Predicting train (debug) ", datetime.now().strftime("%H:%M:%S"))
test[["sentence_id", "token_id", "before", "prediction", "class_pred"]].to_csv(
    "outputs/test.4.3.csv", encoding="UTF-8")
train = predict_df(train)
train[["sentence_id", "token_id", "before", "after", "prediction", "class_pred", "class"]].to_csv(
    "outputs/train.4.3.csv", encoding="UTF-8")

if SESSION == "live":
    tmp_df = train[["before", "after", "prediction", "class_i", "class_pred"]][train["after"] != train["prediction"]]
    tmp_df.to_csv("outputs/not_solved.csv", encoding="UTF-8")
print("Done ", datetime.now().strftime("%H:%M:%S"))
