import numpy as np


my_class = "VERBATIM"


def build_verbatim_char_dict(train):
    verbatim_char_dict = {}
    train_candidates = train[train["class"] == my_class]
    train_candidates = train_candidates[train_candidates["before"] != train_candidates["after"]]
    for i, possible_key in enumerate(train_candidates["before"].values):
        if len(possible_key) == 1:
            if possible_key not in verbatim_char_dict:
                verbatim_char_dict[possible_key] = train_candidates["after"].iat[i]
    return verbatim_char_dict


def build_verbatim_letters_value_counts_series(train, limit):
    verbatim = train["before"][train["class"] == "VERBATIM"]
    string_len = verbatim.map(lambda x: len(x))
    verbatim = verbatim[string_len > 1]
    verbatim_vc = verbatim.value_counts()
    verbatim_vc = verbatim_vc[verbatim_vc > limit]
    return verbatim_vc


def in_value_counts(before_series, vc_series):
    vc = np.zeros(before_series.size)
    vc_index = vc_series.index.values
    for i, value in enumerate(before_series.values):
        if value in vc_index:
            vc[i] = vc_series.at[value]
    return vc
