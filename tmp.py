import pandas as pd
import word_parser
import re

# print(word_parser.parse_date("Sun. 2012-06-11"))
train = pd.read_csv("inputs/en_train_amended.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)

db = train[train["class"] == "TELEPHONE"]

db["prediction"] = db["before"].map(lambda x: word_parser.parse_phone(x))
unsolved = (db[db["prediction"] != db["after"]][["before", "after", "prediction"]])

tmp = zip(unsolved["before"].values,
          unsolved["after"].values,
          unsolved["prediction"].values
          )
for a, b, c in tmp:
    print("###")
    print(a)
    print(b)
    print(c)

print(unsolved.shape)
# print(db["before"].at[9741])
# print(db["after"].at[9741])
# print(db["prediction"].at[9741])
