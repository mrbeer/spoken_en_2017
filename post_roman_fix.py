import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime

import word_parser


def fix_romans(train: pd.DataFrame, test: pd.DataFrame, evaluation: pd.DataFrame, verbose=False):
    train_roman_df = train[train["before"].map(lambda x: bool(re.fullmatch("[IVXLCDM]+\.?'?s?", x)))]
    eval_roman_df = evaluation[evaluation["before"].map(lambda x: bool(re.fullmatch("[IVXLCDM]+\.?'?s?", x)))]
    test_roman_df = test[test["before"].map(lambda x: bool(re.fullmatch("[IVXLCDM]+\.?'?s?", x)))]

    def sum_errors(df):
        return np.sum(df["after"] != df["prediction"])

    print(sum_errors(train_roman_df))
    if verbose:
        print(train_roman_df["class_pred"].value_counts())

    train_roman_df["roman_target"] = 0
    for i in range(train_roman_df.shape[0]):
        if train_roman_df["after"].iat[i] == word_parser.parse_plain(train_roman_df["before"].iat[i]):
            train_roman_df["roman_target"].iat[i] = 0
        elif train_roman_df["after"].iat[i] == word_parser.parse_cardinal(train_roman_df["before"].iat[i]):
            train_roman_df["roman_target"].iat[i] = 1
        elif train_roman_df["after"].iat[i] == word_parser.parse_ordinals(train_roman_df["before"].iat[i]):
            train_roman_df["roman_target"].iat[i] = 2
        elif train_roman_df["after"].iat[i] == word_parser.parse_letters(train_roman_df["before"].iat[i]):
            train_roman_df["roman_target"].iat[i] = 3
        else:
            train_roman_df["roman_target"].iat[i] = 4
    eval_roman_df["roman_target"] = 0
    for i in range(eval_roman_df.shape[0]):
        if eval_roman_df["after"].iat[i] == word_parser.parse_plain(eval_roman_df["before"].iat[i]):
            eval_roman_df["roman_target"].iat[i] = 0
        elif eval_roman_df["after"].iat[i] == word_parser.parse_cardinal(eval_roman_df["before"].iat[i]):
            eval_roman_df["roman_target"].iat[i] = 1
        elif eval_roman_df["after"].iat[i] == word_parser.parse_ordinals(eval_roman_df["before"].iat[i]):
            eval_roman_df["roman_target"].iat[i] = 2
        elif eval_roman_df["after"].iat[i] == word_parser.parse_letters(eval_roman_df["before"].iat[i]):
            eval_roman_df["roman_target"].iat[i] = 3
        else:
            eval_roman_df["roman_target"].iat[i] = 4

    if verbose:
        print(train_roman_df["roman_target"].value_counts())

    print(sum_errors(train_roman_df))

    def print_sentences(sentences_id_arr, n):
        print("There are %d words" % sentences_id_arr.size)
        n = min(n, sentences_id_arr.size)
        for i in range(n):
            sentence = train[train["sentence_id"] == sentences_id_arr[i]]
            words = sentence["before"].values.tolist()
            words_after = sentence["after"].values.tolist()
            class_pred = sentence["class_pred"].values.astype(str).tolist()
            print(i)
            print(" ".join(words))
            print(" ".join(words_after))
            print(" ".join(class_pred))

    n_sentences = 10
    if verbose:
        print("\nPrinting 3 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_roman_df["sentence_id"][train_roman_df["roman_target"] == 3].values, n_sentences)
        print("\nPrinting 2 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_roman_df["sentence_id"][train_roman_df["roman_target"] == 2].values, n_sentences)
        print("\nPrinting 1 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_roman_df["sentence_id"][train_roman_df["roman_target"] == 1].values, n_sentences)
        print("\nPrinting 0 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_roman_df["sentence_id"][train_roman_df["roman_target"] == 0].values, n_sentences)

    def neigh_classes(full_df, df):
        df["before_class"] = -1
        df["before2_class"] = -1
        df["after_class"] = -1
        df["after2_class"] = -1
        indices = df.index.values
        tmp = full_df[["class_pred", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 2 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 2, 1]:
                    df["before2_class"].iat[i] = tmp[index_i - 2, 0]
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    df["before_class"].iat[i] = tmp[index_i - 1, 0]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    df["after_class"].iat[i] = tmp[index_i + 1, 0]
            if index_i + 2 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 2, 1]:
                    df["after2_class"].iat[i] = tmp[index_i + 2, 0]
        return df

    def neigh_number(full_df, df):
        df["before_number"] = -1
        df["after_number"] = -1
        indices = df.index.values
        tmp = full_df[["before", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i - 1, 0]):
                        df["before_number"].iat[i] = int(tmp[index_i - 1, 0])
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i + 1, 0]):
                        df["after_number"].iat[i] = int(tmp[index_i + 1, 0])
        return df

    def before_x(full_df, df):
        pre_cardinal_series = pd.Series.from_csv("outputs/before_cardinals.csv")
        pre_cardinal_index = pre_cardinal_series.index.values
        pre_ordinal_series = pd.Series.from_csv("outputs/before_ordinals.csv")
        pre_ordinal_index = pre_ordinal_series.index.values
        after_cardinal_series = pd.Series.from_csv("outputs/after_cardinals.csv")
        after_cardinal_index = after_cardinal_series.index.values
        after_ordinal_series = pd.Series.from_csv("outputs/after_ordinals.csv")
        after_ordinal_index = after_ordinal_series.index.values
        df["before_cardinal_freq"] = 0
        df["before_ordinal_freq"] = 0
        df["after_cardinal_freq"] = 0
        df["after_ordinal_freq"] = 0
        indices = df.index.values
        tmp = full_df["before"].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i - 1].lower() in pre_cardinal_index:
                    df["before_cardinal_freq"].iat[i] = pre_cardinal_series[tmp[index_i - 1].lower()]
                if tmp[index_i - 1].lower() in pre_ordinal_index:
                    df["before_ordinal_freq"].iat[i] = pre_ordinal_series[tmp[index_i - 1].lower()]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i + 1].lower() in after_cardinal_index:
                    df["after_cardinal_freq"].iat[i] = after_cardinal_series[tmp[index_i + 1].lower()]
                if tmp[index_i + 1].lower() in after_ordinal_index:
                    df["after_ordinal_freq"].iat[i] = after_ordinal_series[tmp[index_i + 1].lower()]
        df["before_ordinal_cardinal_diff"] = df["before_cardinal_freq"].values - df["before_ordinal_freq"].values
        df["after_ordinal_cardinal_diff"] = df["after_cardinal_freq"].values - df["after_ordinal_freq"].values
        return df

    train_roman_df = neigh_classes(train, train_roman_df)
    eval_roman_df = neigh_classes(evaluation, eval_roman_df)
    test_roman_df = neigh_classes(test, test_roman_df)

    train_roman_df = before_x(train, train_roman_df)
    eval_roman_df = before_x(evaluation, eval_roman_df)
    test_roman_df = before_x(test, test_roman_df)

    train_roman_df = neigh_number(train, train_roman_df)
    eval_roman_df = neigh_number(evaluation, eval_roman_df)
    test_roman_df = neigh_number(test, test_roman_df)

    roman_values = {'M': 1000, 'D': 500, 'C': 100, 'L': 50,
                    'X': 10, 'V': 5, 'I': 1}

    def roman_to_int(string):
        """Convert from Roman numerals to an integer."""
        roman = []
        for char in string:
            if char in roman_values:
                roman.append(char)
        numbers = [roman_values[char] for char in roman]
        total = 0
        if len(numbers) > 1:
            for num1, num2 in zip(numbers, numbers[1:]):
                if num1 >= num2:
                    total += num1
                else:
                    total -= num1
            return total + num2
        else:
            return numbers[0]

    train_roman_df["value"] = train_roman_df["before"].map(lambda x: roman_to_int(x))
    eval_roman_df["value"] = eval_roman_df["before"].map(lambda x: roman_to_int(x))
    test_roman_df["value"] = test_roman_df["before"].map(lambda x: roman_to_int(x))

    train_roman_df["hasSign1"] = train_roman_df["before"].map(lambda x: "." in x)
    eval_roman_df["hasSign1"] = eval_roman_df["before"].map(lambda x: "." in x)
    test_roman_df["hasSign1"] = test_roman_df["before"].map(lambda x: "." in x)

    train_roman_df["hasSign2"] = train_roman_df["before"].map(lambda x: "s" in x)
    eval_roman_df["hasSign2"] = eval_roman_df["before"].map(lambda x: "s" in x)
    test_roman_df["hasSign2"] = test_roman_df["before"].map(lambda x: "s" in x)

    train_roman_df["hasSign3"] = train_roman_df["before"].map(lambda x: "'s" in x)
    eval_roman_df["hasSign3"] = eval_roman_df["before"].map(lambda x: "'s" in x)
    test_roman_df["hasSign3"] = test_roman_df["before"].map(lambda x: "'s" in x)

    num_map = [(1000, 'M'), (900, 'CM'), (500, 'D'), (400, 'CD'), (100, 'C'), (90, 'XC'),
               (50, 'L'), (40, 'XL'), (10, 'X'), (9, 'IX'), (5, 'V'), (4, 'IV'), (1, 'I')]

    def num2roman(num):

        roman = ''

        while num > 0:
            for i, r in num_map:
                while num >= i:
                    roman += r
                    num -= i

        return roman

    train_roman_df["valid"] = 0
    eval_roman_df["valid"] = 0
    test_roman_df["valid"] = 0
    for i in range(train_roman_df.shape[0]):
        if num2roman(
                train_roman_df["value"].iat[i]) == train_roman_df["before"].iat[i]:
            train_roman_df["valid"].iat[i] = 1
    for i in range(eval_roman_df.shape[0]):
        if num2roman(
                eval_roman_df["value"].iat[i]) == eval_roman_df["before"].iat[i]:
            eval_roman_df["valid"].iat[i] = 1
    for i in range(test_roman_df.shape[0]):
        if num2roman(
                test_roman_df["value"].iat[i]) == test_roman_df["before"].iat[i]:
            test_roman_df["valid"].iat[i] = 1

    train_indices = train_roman_df.index.values
    eval_indices = eval_roman_df.index.values
    test_indices = test_roman_df.index.values
    train_roman_df["isName"] = 0
    eval_roman_df["isName"] = 0
    test_roman_df["isName"] = 0
    for i in range(1, train_roman_df.shape[0]):
        if bool(re.fullmatch("[A-Z][a-z]+", train["before"].at[train_indices[i] - 1])):
            train_roman_df["isName"].iat[i] = 1
    for i in range(1, eval_roman_df.shape[0]):
        if bool(re.fullmatch("[A-Z][a-z]+", evaluation["before"].at[eval_indices[i] - 1])):
            eval_roman_df["isName"].iat[i] = 1
    for i in range(1, test_roman_df.shape[0]):
        if bool(re.fullmatch("[A-Z][a-z]+", test["before"].at[test_indices[i] - 1])):
            test_roman_df["isName"].iat[i] = 1

    train_roman_df["isPreNum"] = 0
    eval_roman_df["isPreNum"] = 0
    test_roman_df["isPreNum"] = 0
    prenum_re = "volume|division|vol|war|part|chapter|episode|article|series|act|scene"
    for i in range(1, train_roman_df.shape[0]):
        if bool(re.fullmatch(prenum_re, train["before"].at[train_indices[i] - 1], flags=re.IGNORECASE)):
            train_roman_df["isPreNum"].iat[i] = 1
    for i in range(1, eval_roman_df.shape[0]):
        if bool(re.fullmatch(prenum_re, evaluation["before"].at[eval_indices[i] - 1], flags=re.IGNORECASE)):
            eval_roman_df["isPreNum"].iat[i] = 1
    for i in range(1, test_roman_df.shape[0]):
        if bool(re.fullmatch(prenum_re, test["before"].at[test_indices[i] - 1], flags=re.IGNORECASE)):
            test_roman_df["isPreNum"].iat[i] = 1

    train_roman_df["length"] = train_roman_df["before"].map(lambda x: len(x))
    eval_roman_df["length"] = eval_roman_df["before"].map(lambda x: len(x))
    test_roman_df["length"] = test_roman_df["before"].map(lambda x: len(x))

    xg_train = xgb.DMatrix(
        train_roman_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "value",
                        "length", "token_id", "valid", "isName", "isPreNum", "hasSign1", "hasSign2", "hasSign3",
                        "before_cardinal_freq", "before_ordinal_freq", "before_ordinal_cardinal_diff",
                        "after_cardinal_freq", "after_ordinal_freq", "after_ordinal_cardinal_diff"]].values,
        label=train_roman_df["roman_target"].values)
    xg_eval = xgb.DMatrix(
        eval_roman_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "value",
                       "length", "token_id", "valid", "isName", "isPreNum", "hasSign1", "hasSign2", "hasSign3",
                       "before_cardinal_freq", "before_ordinal_freq", "before_ordinal_cardinal_diff",
                       "after_cardinal_freq", "after_ordinal_freq", "after_ordinal_cardinal_diff"]].values,
        label=eval_roman_df["roman_target"].values)
    xg_test = xgb.DMatrix(
        test_roman_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "value",
                       "length", "token_id", "valid", "isName", "isPreNum", "hasSign1", "hasSign2", "hasSign3",
                       "before_cardinal_freq", "before_ordinal_freq", "before_ordinal_cardinal_diff",
                       "after_cardinal_freq", "after_ordinal_freq", "after_ordinal_cardinal_diff"]].values)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'multi:softmax'
    param['eval_metric'] = 'merror'
    # scale weight of positive examples
    param['eta'] = 0.03
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4
    param['num_class'] = 5

    watchlist = [(xg_train, 'train'), (xg_eval, 'eval')]
    num_round = 1000
    bst = xgb.train(param, xg_train, num_round, watchlist)

    # get prediction
    pred_train = bst.predict(xg_train)
    pred_test = bst.predict(xg_test)

    print(np.sum(pred_train != train_roman_df["roman_target"]))

    if verbose:
        print(train_roman_df[["before", "after", "prediction", "roman_target"]][pred_train !=
                                                                                train_roman_df["roman_target"]])

        wrong_prediction = train_roman_df[pred_train != train_roman_df["roman_target"]]
        print("\nPrinting letters sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["roman_target"] == 3].values, n_sentences)
        print("\nPrinting ordinals sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["roman_target"] == 2].values, n_sentences)
        print("\nPrinting cardinals sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["roman_target"] == 1].values, n_sentences)
        print("\nPrinting plain sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["roman_target"] == 0].values, n_sentences)

    print("Fixing")
    for i, index_i in enumerate(train_indices):
        if pred_train[i] == 3:
            train["prediction"].iat[index_i] = word_parser.parse_letters(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 7
        elif pred_train[i] == 2:
            train["prediction"].iat[index_i] = word_parser.parse_ordinals(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 10
        elif pred_train[i] == 1:
            train["prediction"].iat[index_i] = word_parser.parse_cardinal(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 1
        elif pred_train[i] == 0:
            train["prediction"].iat[index_i] = word_parser.parse_plain(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 11
    for i, index_i in enumerate(test_indices):
        if pred_test[i] == 3:
            test["prediction"].iat[index_i] = word_parser.parse_letters(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 7
        elif pred_test[i] == 2:
            test["prediction"].iat[index_i] = word_parser.parse_ordinals(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 10
        elif pred_test[i] == 1:
            test["prediction"].iat[index_i] = word_parser.parse_cardinal(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 1
        elif pred_test[i] == 0:
            test["prediction"].iat[index_i] = word_parser.parse_plain(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 11

    return train, test

# 917-> 314
# 1164 -> 384
