import numpy as np


def full_match(train, test, re_expression, my_class, show_undetected=False):
    train_subject = train["before"].map(lambda x: int(bool(re_expression.fullmatch(x))))
    test_subject = test["before"].map(lambda x: int(bool(re_expression.fullmatch(x))))
    print("type 1 error(false negative): {0}".format(1 - np.mean(train_subject[train["class"].values == my_class])))
    print("type 2 error(false positive): {0}".format(np.mean(train_subject[train["class"].values != my_class])))
    if show_undetected:
        train["tmp"] = train_subject
        filtered = train[train["class"] == my_class]
        print(filtered[filtered["tmp"] == 0])
    return train_subject, test_subject


def count_regex(train, test, re_expression):
    train_subject = train["before"].map(lambda x: len(re_expression.findall(x)))
    test_subject = test["before"].map(lambda x: len(re_expression.findall(x)))
    return train_subject, test_subject
