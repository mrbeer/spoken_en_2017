import re
import numpy as np
import pandas as pd
import csv
from nltk.stem.snowball import SnowballStemmer
from glob import glob

stemmer = SnowballStemmer("english")


def todo_dicter(train):
    df = train[train["class"] == "PLAIN"]
    df = df[df["before"] != df["after"]]
    df["before_stemmed"] = df["before"].map(lambda x: stemmer.stem(x))
    df["after_stemmed"] = df["after"].map(lambda x: stemmer.stem(x))
    df = df[["before_stemmed", "after_stemmed"]]

    plain_todo_dict = {}
    for line in df.values:
        if line[0] not in plain_todo_dict:
            plain_todo_dict[line[0]] = [line[1]]
        else:
            if line[1] not in plain_todo_dict[line[0]]:
                plain_todo_dict[line[0]].append(line[1])
    return plain_todo_dict


def todo_dicter_v2(n=-1):
    file_list = sorted(glob("inputs/en_with_types/*"))
    plain = []
    word_i = 0
    for file in file_list:
        # print(file)
        with open(file, encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                if word_i == n:
                    break
                if len(row) == 3:
                    if row[0] == "PLAIN" and row[2] != "<self>":
                        plain.append(row[1:])
                    word_i += 1
    df = pd.DataFrame(plain)
    df.columns = ["before", "after"]
    df["before_stemmed"] = df["before"].map(lambda x: stemmer.stem(x))
    df["after_stemmed"] = df["after"].map(lambda x: stemmer.stem(x))
    df = df[["before_stemmed", "after_stemmed"]]

    plain_todo_dict = {}
    for line in df.values:
        if line[0] not in plain_todo_dict:
            plain_todo_dict[line[0]] = [line[1]]
        else:
            if line[1] not in plain_todo_dict[line[0]]:
                plain_todo_dict[line[0]].append(line[1])
    return plain_todo_dict


def orour_dicter(todo_dict):
    orour_dict = {}
    for key in todo_dict:
        if key.split("our") == todo_dict[key][0].split("or") and "our" in key:
            orour_dict[key] = todo_dict[key]
    return orour_dict


def mmem_dicter(todo_dict):
    mmem_dict = {}
    for key in todo_dict:
        if key[-2:] == "mm" and key[:-1] == todo_dict[key][0]:
            mmem_dict[key] = todo_dict[key]
    return mmem_dict


def gueg_dicter(todo_dict):
    gueg_dict = {}
    for key in todo_dict:
        if key[-2:] == "gu" and key[:-1] == todo_dict[key][0]:
            gueg_dict[key] = todo_dict[key]
    return gueg_dict


def isiz_dicter(todo_dict):
    isiz_dict = {}
    for key in todo_dict:
        if (key[-2:] == "is" and key[:-2] == todo_dict[key][0]) or \
                (key[-2:] == "ys" and todo_dict[key][0][-2:] == "yz") or \
                (key[-4:] == "alis" and key[:-4] == todo_dict[key][0]) or \
                (key[-2:] == "is" and todo_dict[key][0][-2:] == "iz"):
            isiz_dict[key] = todo_dict[key]
    return isiz_dict


def short_dicter(todo_dict):
    short_dict = {}
    for key in todo_dict:
        if len(key) < len(todo_dict[key][0]) and ")" not in key:
            if todo_dict[key][0].split()[-1] not in ["weg", "strass"]:
                short_dict[key] = todo_dict[key]
    return short_dict


def reer_dicter(todo_dict):
    reer_dict = {}
    for key in todo_dict:
        if (key[-1] == "r" and key[:-1] == todo_dict[key][0]) or \
                (key[-1] == "r" and todo_dict[key][0][-2:] == "er" and key[:-1] == todo_dict[key][0][:-2]):
            reer_dict[key] = todo_dict[key]
    return reer_dict


def is_capital_stuck(df):
    words = df["before"].values
    sentence_ids = df["sentence_id"].values

    sentences = [[words[0]]]
    sentence_unique_ids = [sentence_ids[0]]
    for i in np.arange(1, words.size):
        if sentence_ids[i] != sentence_ids[i - 1]:
            sentences.append([words[i]])
            sentence_unique_ids.append(sentence_ids[i])
        else:
            sentences[-1].append(words[i])

    sentences_cap = []
    for i, sentence in enumerate(sentences):
        caps = True
        alpha = False
        for word in sentence:
            if word.isalpha():
                alpha = True
                if not word.isupper():
                    caps = False

        if caps and alpha:
            sentences_cap.append(sentence_unique_ids[i])

    df["isCapital_locked"] = 0
    for sentence_i in sentences_cap:
        index = df[df["sentence_id"] == sentence_i].index.values
        for i in index:
            df["isCapital_locked"].at[i] = 1
    return df


def auoei_ratio(string: str):
    string_list = re.split("[.& -]", string)
    string = "".join(string_list)
    if len(re.findall("[a-zA-Z]", string)):
        return len(re.findall("[aueoiAUEOI]", string)) / len(re.findall("[a-zA-Z]", string))
    else:
        return -1


def in_value_counts(before_series, vc_series):
    vc = np.zeros(before_series.size)
    vc_index = vc_series.index.values
    for i, value in enumerate(before_series.values):
        if value.lower() in vc_index:
            vc[i] = vc_series.at[value.lower()]
    return vc
