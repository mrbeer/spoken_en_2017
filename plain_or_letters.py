import numpy as np
import pandas as pd
import re

import pyximport

pyximport.install()
import word_context

# import context_dict
train = pd.read_csv("inputs/en_train_amended.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)

detection_filter = re.compile("[a-zé']{2,}", flags=re.IGNORECASE)
plain = train["before"][train["class"] == "PLAIN"]
plain = plain[plain.map(lambda x: bool(detection_filter.fullmatch(x)))]
letters = train["before"][train["class"] == "LETTERS"]
letters = letters[letters.map(lambda x: bool(detection_filter.fullmatch(x)))]

n_items = 10000
context_list = word_context.create_context_valuecounts(plain.iloc[:n_items].values)
plain_context_value_counts = pd.Series(context_list).value_counts(normalize=True)
plain_context_value_counts_index = plain_context_value_counts.index.values
print(plain_context_value_counts)
context_list = word_context.create_context_valuecounts(letters.iloc[:n_items].values)
letters_context_value_counts = pd.Series(context_list).value_counts(normalize=True)
letters_context_value_counts_index = letters_context_value_counts.index.values
print(letters_context_value_counts)

plain = pd.DataFrame(plain)
plain["plain_context_score"] = 0.0
plain["letters_context_score"] = 0.0
letters = pd.DataFrame(letters)
letters["plain_context_score"] = 0.0
letters["letters_context_score"] = 0.0

for i, word in enumerate(letters["before"].values):
    word_plain_score = 0
    word_letters_score = 0
    for j in range(len(word) - 1):
        context = word[j:j + 2].lower()
        if context in plain_context_value_counts_index:
            word_plain_score += plain_context_value_counts.at[context]
        else:
            word_plain_score = 0
        if context in letters_context_value_counts_index:
            word_letters_score += letters_context_value_counts.at[context]
        else:
            word_letters_score = 0
    if word_plain_score:
        letters["plain_context_score"].iat[i] = word_plain_score
    if word_letters_score:
        letters["letters_context_score"].iat[i] = word_letters_score

print(letters)
print(np.mean(np.log(1 + letters["plain_context_score"].values)))
print(np.mean(np.log(1 + letters["letters_context_score"].values)))

for i, word in enumerate(plain["before"].values):
    word_plain_score = 0
    word_letters_score = 0
    for j in range(len(word) - 1):
        context = word[j:j + 2].lower()
        if context in plain_context_value_counts_index:
            word_plain_score += plain_context_value_counts.at[context]
        else:
            word_plain_score = 0
        if context in letters_context_value_counts_index:
            word_letters_score += letters_context_value_counts.at[context]
        else:
            word_letters_score = 0
    if word_plain_score:
        plain["plain_context_score"].iat[i] = word_plain_score
    if word_letters_score:
        plain["letters_context_score"].iat[i] = word_letters_score

print(plain)
print(np.mean(np.log(1 + plain["plain_context_score"].values)))
print(np.mean(np.log(1 + plain["letters_context_score"].values)))

print(plain.size)
print(letters.size)
#
#
# def research_filter(string_filter, show_n):
#     print("***")
#     print(string_filter)
#     re_filter = re.compile(string_filter, flags=re.IGNORECASE)
#     plain_filtered = plain.map(lambda x: bool(re_filter.fullmatch(x)))
#     letters_filtered = letters.map(lambda x: bool(re_filter.fullmatch(x)))
#
#     print(np.mean(plain_filtered))
#     print(np.mean(letters_filtered))
#
#     print(plain[~plain_filtered].values[:show_n].tolist())
#
#
# not_vowel = "([bcdfghjklmnpqrstvwxyz]|th|st|ch|sh|ght?|ss)"
# vowel = "([auioeé]|ea|ou|ee|ai|oa|ui|ei|ie|ue)"
# research_filter(not_vowel + "?" + "(" + vowel + not_vowel + "|i[oa]n)+[éey]?", 50)
# research_filter(not_vowel + "(" + vowel + not_vowel + "|i[oa]n)+[éey]?", 50)
#
# not_vowel = "([bcdfghjklmnpqrstvwxyz]|thr?|st|ch|sh|ght?|ss)"
# vowel = "([auioeé]|ea|ou|ee|ai|oa|ui|ei|ie|ue)"
# research_filter(not_vowel + "?" + "(" + vowel + not_vowel + "|i[oa]n)+([éeys]|ies)?", 50)
# research_filter(not_vowel + "(" + vowel + not_vowel + "|i[oa]n)+([éeys]|ies)?", 50)
#
# not_vowel = "([bcdfghjklmnpqrstvwxyz]|thr?|st|ch|sh|ght?|ss)"
# vowel = "([auioeé]|ea|ou|ee|ai|oa|ui|ei|ie|ue)"
# research_filter(not_vowel + "{1,2}" + "(" + vowel + not_vowel + "{1,2}" + "|i[oa]n)+([éeys]|ies)?", 50)
# research_filter(not_vowel + "{0,2}" + "(" + vowel + not_vowel + "{1,2}" + "|i[oa]n)+([éeys]|ies)?", 50)
