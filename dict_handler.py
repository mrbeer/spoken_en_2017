import pandas as pd
import re
import csv


def create_vc_series(filename, re_filter, n_values, n_words=-1):
    word_list = []
    word_i = 0
    done_reading = False
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            for word in row:
                if word_i == n_words:
                    done_reading = True
                    break
                word = remove_junc(word)
                if re.fullmatch(re_filter, word):
                    word_list.append(word)
                word_i += 1
            if done_reading:
                break
    print("read %d words" % word_i)
    value_counts_series = pd.Series(word_list).value_counts()
    if value_counts_series.size > n_values:
        value_counts_series = value_counts_series.iloc[:n_values]
    print("dict size {0}".format(value_counts_series.size))
    value_counts_series = value_counts_series.sort_index()
    return value_counts_series


def search_sorted_series(string, sorted_vc):
    string = remove_junc(string)
    if string in sorted_vc.index.values:
        return sorted_vc.at[string]
    else:
        return 0
        # compare index to string
        # if found return value count. else return 0.


def remove_junc(string: str):
    while len(string) > 1 and string[-1] in [",", "-", " "] and not bool(re.fullmatch("-+", string)):
        string = string[:-1]
    return string


if __name__ == '__main__':
    vc = create_vc_series("outputs/letters.csv", "[A-Z]+", 100)
    print(vc.index.values.tolist())
    vc = create_vc_series("outputs/plain.csv", "[A-Z]+", 100)
    print(vc.index.values.tolist())
