import pandas as pd
import numpy as np
import re
from datetime import datetime

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD
from sklearn import preprocessing

import word_parser
import verbatim_lib
import plain_helpers

from word_parser import build_plain_dictionaries
from plain_helpers import is_capital_stuck, auoei_ratio
from verbatim_lib import build_verbatim_letters_calue_counts_series, in_value_counts
from re_filters import full_match, count_regex
from word_counting import sentence_word_counter_dict, sentence_word_counter, token_normaliser, \
    sentence_alpha_word_counter_dict
from regex_functions import string_to_cardinal

print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("inputs/en_train.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)
# """Trials"""
# train = train.iloc[:100000]
# """Trials end"""

test = pd.read_csv("inputs/en_test.csv", dtype=str)
test["before"] = test["before"].astype(str)

print("Digit features ", datetime.now().strftime("%H:%M:%S"))
train["firstZero"] = train["before"].map(lambda x: x[0] == "0").astype(int)
test["firstZero"] = test["before"].map(lambda x: x[0] == "0").astype(int)

train["cardinalValue"] = train["before"].map(lambda x: string_to_cardinal(x)).astype(int)
test["cardinalValue"] = test["before"].map(lambda x: string_to_cardinal(x)).astype(int)

print("AUOEI letters ratio ", datetime.now().strftime("%H:%M:%S"))
train["auoeiRatio"] = train["before"].map(lambda x: auoei_ratio(x))
test["auoeiRatio"] = test["before"].map(lambda x: auoei_ratio(x))

print("Plain dict analysis ", datetime.now().strftime("%H:%M:%S"))
short_plain = train[train["class"] == "PLAIN"]["before"]
short_plain = short_plain[short_plain.map(lambda x: len(x) > 1)]
short_plain = short_plain.map(lambda x: "".join(re.split("[ &.-]", x)).lower())
short_plain = short_plain[short_plain.map(lambda x: len(x) < 4)]
short_plain_vc = short_plain.value_counts()
short_plain_dict = short_plain_vc[short_plain_vc > 50]
train["plainDict"] = plain_helpers.in_value_counts(train["before"], short_plain_vc)
test["plainDict"] = plain_helpers.in_value_counts(test["before"], short_plain_vc)

print("Letters dict analysis ", datetime.now().strftime("%H:%M:%S"))
letters = train[train["class"] == "LETTERS"]["before"]
letters = letters[letters.map(lambda x: len(x) > 1)]
letters["simp"] = letters.map(lambda x: "".join(re.split("[ &.-]", x)).lower())
letters_vc = letters["simp"].value_counts()
letters_dict = letters_vc[letters_vc > 25]
train["lettersDict"] = plain_helpers.in_value_counts(train["before"], letters_dict)
test["lettersDict"] = plain_helpers.in_value_counts(test["before"], letters_dict)

train["plainLettersDif"] = train["plainDict"].values - train["lettersDict"].values
test["plainLettersDif"] = test["plainDict"].values - test["lettersDict"].values

print("Check for caps sentences ", datetime.now().strftime("%H:%M:%S"))
train = is_capital_stuck(train)
test = is_capital_stuck(test)

print("Verbatim dict analysis ", datetime.now().strftime("%H:%M:%S"))
verbatim_vc = build_verbatim_letters_calue_counts_series(train, 10)

train["verbatim_vc"] = in_value_counts(train["before"], verbatim_vc)
test["verbatim_vc"] = in_value_counts(test["before"], verbatim_vc)

print("Building Plain transformations dict ", datetime.now().strftime("%H:%M:%S"))
build_plain_dictionaries(train)

print("Counting words ", datetime.now().strftime("%H:%M:%S"))
train_sentence_word_count_dict = sentence_word_counter_dict(train)
test_sentence_word_count_dict = sentence_word_counter_dict(test)

train["sentence_n_words"] = sentence_word_counter(train[["sentence_id"]], train_sentence_word_count_dict)
test["sentence_n_words"] = sentence_word_counter(test[["sentence_id"]], test_sentence_word_count_dict)

print("Normalising tokens ", datetime.now().strftime("%H:%M:%S"))
train["normalised_tokens"] = token_normaliser(train[["token_id", "sentence_n_words"]])
test["normalised_tokens"] = token_normaliser(test[["token_id", "sentence_n_words"]])

print("Counting alpha words ", datetime.now().strftime("%H:%M:%S"))
train_sentence_alpha_word_count_dict = sentence_alpha_word_counter_dict(train)
test_sentence_alpha_word_count_dict = sentence_alpha_word_counter_dict(test)

train["sentenceAlphawords"] = sentence_word_counter(train[["sentence_id"]], train_sentence_word_count_dict)
test["sentenceAlphawords"] = sentence_word_counter(test[["sentence_id"]], test_sentence_word_count_dict)

print("Basic parameters ", datetime.now().strftime("%H:%M:%S"))
train["nDigits"], test["nDigits"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[0-9]"))

train["nCapitals"], test["nCapitals"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[A-Z]"))

train["nSmalls"], test["nSmalls"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[a-z]"))

train["nLetters"], test["nLetters"] = count_regex(
    train[["before"]], test[["before"]], re.compile("[a-zA-Z]"))

train["length"] = train["before"].map(lambda x: len(x))
test["length"] = test["before"].map(lambda x: len(x))

print("Basic split parameters ", datetime.now().strftime("%H:%M:%S"))
print("address")
address_pattern = re.compile("(Interstate|U\.?S\.?|SR|CR|[A-Z])[ -]?[0-9]+,? ?")
train["isAddress"], test["isAddress"] = full_match(
    train[["before", "class"]], test[["before"]], address_pattern, "ADDRESS")

print("cardinals")
cardinal_pattern = re.compile("-?[1-9]+[0-9]*([, ][0-9]{3})*[,:-]? ?(U\.?S\.? )?|0[0-9]?,?|" +
                              "(X{,3}?I?V?I{1,3}|X{,3}?I?V|X{,3}?I?X)\.?('s)? ?")
train["isCardinal"], test["isCardinal"] = full_match(
    train[["before", "class"]], test[["before"]], cardinal_pattern, "CARDINAL")

print("date")
weekdays_re = "((Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday),? )"
months_re = "(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|June?|" + \
            "July?|Aug(ust)?|Sept?(ember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\.?,?"
months_num_dict = {1: "january", 2: "february", 3: "march", 4: "april",
                   5: "may", 6: "june", 7: "july", 8: "august", 9: "september",
                   10: "october", 11: "november", 12: "december"}

date_pattern = re.compile("([0-9]{3,4}('?s)?,? ?|" +
                          "[0-9]{1,4}[/.][0-9]{1,2}[/.][0-9]{1,4}|[0-9]{2,4}-[0-9]{2}-[0-9]{2,4}|" +
                          weekdays_re + "?" + "([0-9]{1,2} )?" + months_re + " [0-9]{3,4}|" +
                          weekdays_re + "?" + months_re + " [0-9]{1,2},? [0-9]{3,4}|" +
                          "([0-9]{1,2} )?" + months_re + "( [0-9]{1,2})?).?|" +
                          "[0-9]{1,4} (B\.?C\.?|A\.?D\.?|(B\.?)?C\.?E\.?)|[0-9]{2}'?s")

train["isDate"], test["isDate"] = full_match(
    train[["before", "class"]], test[["before"]], date_pattern, "DATE")

print("decimal")
decimal_pattern = re.compile("-?[0-9]{,3}(,?[0-9]{3})*\.[0-9]+( million| billion| thousand| trillion)?|" +
                             "[0-9]{,3}(,?[0-9]{3})*( million| billion| thousand| trillion)")
train["isDecimal"], test["isDecimal"] = full_match(
    train[["before", "class"]], test[["before"]], decimal_pattern, "DECIMAL")

print("digit")
digits_pattern = re.compile("([0-9] )*[0-9]+-?,? ?")
train["isDigits"], test["isDigits"] = full_match(
    train[["before", "class"]], test[["before"]], digits_pattern, "DIGIT")

print("electronic")
electronic_pattern = re.compile(
    "#[A-Za-z0-9-]+|::|" +
    "(//)?([A-Za-z0-9-]+\.)+(com|org|at|uk|it|edu|pl|net|fi|html|gr|tv|jp|cz|nl|ru|ca|gov).*|" +
    ".*https?://.*|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}|[a-zA-Z]*(\.[a-z]+)+")
train["isElectronic"], test["isElectronic"] = full_match(
    train[["before", "class"]], test[["before"]], electronic_pattern, "ELECTRONIC")

print("fraction")
fraction_pattern = re.compile("-? ?[0-9]* ?[0-9]+ ?/ ?[0-9]+")
train["isFraction"], test["isFraction"] = full_match(
    train[["before", "class"]], test[["before"]], fraction_pattern, "FRACTION")

print("letters")
letters_pattern = re.compile("([A-Z]\.? ?)*[A-Z]+[s.-]? ?|[a-z]")
train["isLetters"], test["isLetters"] = full_match(
    train[["before", "class"]], test[["before"]], letters_pattern, "LETTERS")

print("measure")
measure_pattern = re.compile("-?[0-9]+(,[0-9]{3})*(\.[0-9]+)? ?(sq )?([μa-zA-Z]{1,3}[23²³]?|%|'|\"|percent)|" +
                             "-?[0-9]+(,[0-9]{3})*(\.[0-9]+)? ?(sq )?[μa-zA-Z]{,3}[23²³]?/[a-zA-Z]{1,3}[23²³]?|" +
                             "(/|per) ?[a-zA-Z]+[23²³]?")
train["isMeasure"], test["isMeasure"] = full_match(
    train[["before", "class"]], test[["before"]], measure_pattern, "MEASURE")

print("money")
money_pattern = re.compile("((US)?A?(NZ)?\$|£|€|¥)[0-9]+(,[0-9]{3})*(.[0-9]{1,3})?" +
                           " ?([kK]|[Tt](rillion)?|[Mm](illion)?|[Bb]n?|[Bb](illion)?)?")
train["isMoney"], test["isMoney"] = full_match(
    train[["before", "class"]], test[["before"]], money_pattern, "MONEY")

print("ordinal")
ordinal_pattern = re.compile("([0-9, ]*(1[Ss][Tt]|2[Nn][Dd]|3[Rr][Dd]|[0-9][Tt][Hh])|" +
                             "(X{,3}?V?I{1,3}|X{,3}?I?V|X{,3}?I?X)(th|\.)?)('?s)?")
train["isOrdinal"], test["isOrdinal"] = full_match(
    train[["before", "class"]], test[["before"]], ordinal_pattern, "ORDINAL")

print("telephone")
phone_pattern = re.compile("[0-9]+|(\(?[0-9]+[) x-]+)+[0-9]+|(\(?[0-9]+[) x-]+)+[A-Z]+")
train["isPhone"], test["isPhone"] = full_match(
    train[["before", "class"]], test[["before"]], phone_pattern, "TELEPHONE")

print("plain")
plain_pattern = re.compile("[A-Z]|[a-zé]*[A-Z]?[a-zé]+('s|s'|'t|'re|'ve)?|-|I'm|([A-Z][a-zé]+)*")
train["isPlain"], test["isPlain"] = full_match(
    train[["before", "class"]], test[["before"]], plain_pattern, "PLAIN")

print("punct")
punct_pattern = re.compile("[.,()—:\"'/¡!?;«»¿\[\]|]|-+")
train["isPunct"], test["isPunct"] = full_match(
    train[["before", "class"]], test[["before"]], punct_pattern, "PUNCT")

print("time")
time_pattern = re.compile(
    "([0-9]{1,2} ?[:.] ?[0-9]{2}( ?[:.] ?[0-9]{1,2})? ?(A\.?M.?|P\.?M.?|a\.?m\.?|p\.?m\.?)?|" +
    "[0-9]{1,2} ?(A\.?M\.?|P\.?M\.?|a\.?m\.?|p\.?m\.?)) ?[A-Z]*")
train["isTime"], test["isTime"] = full_match(
    train[["before", "class"]], test[["before"]], time_pattern, "TIME")

verbatim_char_dict = verbatim_lib.build_verbatim_char_dict(train[["before", "after", "class"]])

pd.Series(np.sum(
    test[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
          "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime"]].values,
    axis=1)).value_counts()

print("Label encoding ", datetime.now().strftime("%H:%M:%S"))
le = preprocessing.LabelEncoder()
le.fit(train["class"].values)
train["class_i"] = le.transform(train["class"].values)

print("First class prediction ", datetime.now().strftime("%H:%M:%S"))


""" Train CNN"""
model = Sequential()
model.add(Dense(64, activation='relu', input_dim=32))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(16, activation='softmax'))

sgd = SGD(lr=0.1, decay=1e-7, momentum=0.7, nesterov=True)
model.compile(optimizer=sgd,
              loss='categorical_crossentropy',
              metrics=['accuracy'])

y_train = keras.utils.to_categorical(train["class_i"].values, num_classes=16)

model.fit(
    train[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
           "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
           "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens",
           "verbatim_vc", "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio",
           "plainLettersDif", "firstZero", "cardinalValue"]].values, y_train,
    epochs=5, batch_size=64, verbose=1)

score = model.evaluate(
    train[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
           "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
           "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens"
                                                                                        "verbatim_vc",
           "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio",
           "plainLettersDif", "firstZero", "cardinalValue"]].values, y_train,
    batch_size=64, verbose=1)
print(score)

class_predictions = model.predict(
    test[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
          "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
          "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens",
          "verbatim_vc", "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio",
          "plainLettersDif", "firstZero", "cardinalValue"]].values, batch_size=64, verbose=1)

class_predictions = np.argmax(class_predictions, axis=1)
print(class_predictions)

test["class_pred"] = class_predictions
# print("Finding context ", datetime.now().strftime("%H:%M:%S"))
#
#
# def neigh_inference(df):
#     df["before1_class_iter"] = -1
#     df["before2_class_iter"] = -1
#     df["after1_class_iter"] = -1
#     df["after2_class_iter"] = -1
#     tmp = df[["class_pred_iter", "sentence_id"]].values
#     for i in np.arange(tmp.shape[0]):
#         if i - 1 >= 0:
#             if tmp[i, 1] == tmp[i - 1, 1]:
#                 df["before1_class_iter"].iat[i] = tmp[i - 1, 0]
#         if i - 2 >= 0:
#             if tmp[i, 1] == tmp[i - 2, 1]:
#                 df["before2_class_iter"].iat[i] = tmp[i - 2, 0]
#         if i + 2 < tmp.shape[0]:
#             if tmp[i, 1] == tmp[i + 2, 1]:
#                 df["after2_class_iter"].iat[i] = tmp[i + 2, 0]
#         if i + 1 < tmp.shape[0]:
#             if tmp[i, 1] == tmp[i + 1, 1]:
#                 df["after1_class_iter"].iat[i] = tmp[i + 1, 0]
#     return df
#
#
# train = neigh_inference(train)
# test = neigh_inference(test)
#
# print("Class prediction with context ", datetime.now().strftime("%H:%M:%S"))
# train["class_pred"], test["class_pred"] = predict_train_test_classes(
#     train[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
#            "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
#            "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens",
#            "before1_class_iter", "before2_class_iter", "after1_class_iter", "after2_class_iter",
#            "verbatim_vc", "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio",
#            "plainLettersDif", "firstZero", "cardinalValue"]].values,
#     train["class_i"].values,
#     test[["isAddress", "isCardinal", "isDate", "isDecimal", "isDigits", "isElectronic", "isFraction", "isLetters",
#           "isLetters", "isMeasure", "isMoney", "isOrdinal", "isPhone", "isPlain", "isPunct", "isTime",
#           "length", "nCapitals", "nLetters", "nSmalls", "nDigits", "sentence_n_words", "normalised_tokens",
#           "before1_class_iter", "before2_class_iter", "after1_class_iter", "after2_class_iter",
#           "verbatim_vc", "isCapital_locked", "sentenceAlphawords", "plainDict", "lettersDict", "auoeiRatio",
#           "plainLettersDif", "firstZero", "cardinalValue"]].values)

print(train["class_i"].value_counts())
# print(train["class_pred"].value_counts())
print(test["class_pred"].value_counts())
print(list(zip(range(16), le.classes_)))

print("Predicting words ", datetime.now().strftime("%H:%M:%S"))


def predict_word(string, str_class):
    if str_class == 0:
        return word_parser.parse_address(string)
    elif str_class == 1:
        return word_parser.parse_cardinal(string)
    elif str_class == 2:
        return word_parser.parse_date(string)
    elif str_class == 3:
        return word_parser.parse_decimal(string)
    elif str_class == 4:
        return word_parser.parse_digits(string)
    elif str_class == 5:
        return word_parser.parse_electronic(string)
    elif str_class == 6:
        return word_parser.parse_fraction(string)
    elif str_class == 7:
        return word_parser.parse_letters(string)
    elif str_class == 8:
        return word_parser.parse_measure(string)
    elif str_class == 9:
        return word_parser.parse_money(string)
    elif str_class == 10:
        return word_parser.parse_ordinals(string)
    elif str_class == 11:
        return word_parser.parse_plain(string)
    elif str_class == 12:
        return word_parser.parse_punct(string)
    elif str_class == 13:
        return word_parser.parse_phone(string)
    elif str_class == 14:
        return word_parser.parse_time(string)
    elif str_class == 15:
        return word_parser.parse_verbatim(string, verbatim_char_dict)


def predict_df(df):
    df["prediction"] = ""
    for i, before in enumerate(df["before"].values):
        df["prediction"].iat[i] = predict_word(before, df["class_pred"].iat[i])
    return df


test = predict_df(test)

submission = pd.DataFrame.from_csv("inputs/en_sample_submission.csv")
submission["after"] = test["prediction"].values
submission.to_csv("outputs/v2.17.cnn.csv")

print("Predicting train (debug) ", datetime.now().strftime("%H:%M:%S"))
train = predict_df(train)

tmp_df = train[["before", "after", "class_i", "class_pred"]][train["after"] != train["prediction"]]
tmp_df.to_csv("outputs/not_solved.csv")
print("Done ", datetime.now().strftime("%H:%M:%S"))
