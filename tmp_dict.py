import pandas as pd
from datetime import datetime
import re
import dict_handler

import pyximport
pyximport.install()
import word_context

script_version = "4.3"
post_version = "20"
print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("outputs/train.{0}.csv".format(script_version))
train["before"] = train["before"].astype(str)
train["prediction"] = train["prediction"].astype(str)
train["after"] = train["after"].astype(str)
train["class_pred"] = train["class_pred"].astype(float).astype(int)

train_caps_df = train[train["before"].map(lambda x: bool(re.fullmatch("[a-z][a-zé']*[ ,-]*", x)))]
print("Plain building dict ", datetime.now().strftime("%H:%M:%S"))
plain_vc = dict_handler.create_vc_series("outputs/plain.csv", "[a-z][a-zé']*[ ,-]*", 5000, 1000000)

print("Plain dict comparing", datetime.now().strftime("%H:%M:%S"))
train_caps_df["plainDict"] = word_context.search_sorted_series(
    train_caps_df["before"].values.tolist(),
    plain_vc.index.values.tolist(),
    plain_vc.values)

print("Plain dict comparing ", datetime.now().strftime("%H:%M:%S"))
train_caps_df["plainDict"] = train_caps_df["before"].map(
    lambda x: dict_handler.search_sorted_series(x, plain_vc))

print("Done", datetime.now().strftime("%H:%M:%S"))
print(train_caps_df)
