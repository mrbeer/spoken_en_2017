import numpy as np


def sentence_word_counter_dict(df):
    sentence_length = {}
    for value in df["sentence_id"].values:
        if value in sentence_length:
            sentence_length[value] += 1
        else:
            sentence_length[value] = 1
    return sentence_length


def sentence_alpha_word_counter_dict(df):
    words = df["before"].values
    sentence_ids = df["sentence_id"].values

    sentences = [[words[0]]]
    sentence_unique_ids = [sentence_ids[0]]
    for i in np.arange(1, words.size):
        if sentence_ids[i] != sentence_ids[i - 1]:
            sentences.append([words[i]])
            sentence_unique_ids.append(sentence_ids[i])
        else:
            sentences[-1].append(words[i])

    sentences_words = {}
    for i, sentence in enumerate(sentences):
        count = 0
        for word in sentence:
            if word.isalpha():
                count += 1
        sentences_words[sentence_unique_ids[i]] = count
    return sentences_words


def sentence_word_counter(df, word_count_dict):
    sentence_n_words = np.zeros((df.shape[0],))
    for i, value in enumerate(df["sentence_id"].values):
        sentence_n_words[i] = word_count_dict[value]
    return sentence_n_words


def token_normaliser(df):
    normalised_tokens = np.zeros((df.shape[0],))
    for i in np.arange(df.shape[0]):
        normalised_tokens[i] = (float(df["token_id"].iat[i]) + 1.0) / df["sentence_n_words"].iat[i]
    return normalised_tokens
