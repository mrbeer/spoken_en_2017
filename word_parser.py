import re
from num2words import num2words
from nltk.stem.snowball import SnowballStemmer
import pandas as pd

import plain_helpers

stemmer = SnowballStemmer("english")


def parse_address(string):
    letters = []
    number = []
    for char in string:
        if char.isalpha():
            letters.append(char.lower())
        if char.isdigit():
            number.append(char)
    if len(letters) and len(number):
        number = "".join(number)
        if len(number) >= 4:
            number = parse_digits(number)
        elif len(number) == 3:
            if number[0] == "0":
                number = parse_cardinal(number[1:])
            else:
                if number[1] == "0":
                    number = parse_digits(number)
                else:
                    number = " ".join([parse_digits(number[0]), parse_cardinal(number[1:])])
        else:
            if number[0] == "0":
                number = parse_digits(number)
            else:
                number = parse_cardinal(number)
        letters = " ".join(letters)
        if letters == "i n t e r s t a t e":
            letters = letters.replace(" ", "")
        return " ".join([letters, number])
    return string


roman_values = {'M': 1000, 'D': 500, 'C': 100, 'L': 50,
                'X': 10, 'V': 5, 'I': 1}

digits_set = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
digits_set = set(digits_set)

re_roman = re.compile("[IVXLCDM]+\.?('s)? ?")


def parse_cardinal(string):
    is_s = False
    if len(string):
        if string[-1] == "s":
            is_s = True
    if re_roman.match(string):
        string = str(_roman_to_int(string))
    after = []
    for char in string:
        if char in digits_set:
            after.append(char)
    if len(after):
        after = _cardinal_to_spoken("".join(after))
        if string[0] == "-":
            after = "minus " + after
        if is_s:
            after += "'s"
        return after
    else:
        return string


def _roman_to_int(string):
    """Convert from Roman numerals to an integer."""
    roman = []
    for char in string:
        if char in roman_values:
            roman.append(char)
    if len(roman):
        numbers = [roman_values[char] for char in roman]
        total = 0
        if len(numbers) > 1:
            for num1, num2 in zip(numbers, numbers[1:]):
                if num1 >= num2:
                    total += num1
                else:
                    total -= num1
            return total + num2
        else:
            return numbers[0]
    else:
        return string


def _cardinal_to_spoken(string):
    middle = num2words(int(string))
    middle = middle.replace(",", "").replace("-", " ")
    middle = middle.split()
    after = []
    for word in middle:
        if word != "and":
            after.append(word)
    return " ".join(after)


weekdays_re = "(Mon(day)?|Tuesday|Wed(nesday)?|Thu(rsday)?|Friday|Sat(urday)?|Sun(day)?),?.?"
months_re = "(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|June?|" + \
            "July?|Aug(ust)?|Sept?(ember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\.?,?"
months_num_dict = {1: "january", 2: "february", 3: "march", 4: "april",
                   5: "may", 6: "june", 7: "july", 8: "august", 9: "september",
                   10: "october", 11: "november", 12: "december"}

months_short_dict = {"Jan": "january", "Feb": "february", "Mar": "march", "Apr": "april",
                     "Jun": "june", "Jul": "july", "Aug": "august", "Sep": "september", "Sept": "september",
                     "Oct": "october", "Nov": "November", "Dec": "december"}

weekdays_short_dict = {"sun": "sunday", "mon": "monday", "wed": "wednesday", "sat": "saturday"}


def parse_date(string: str):
    if len(string) > 4:
        if string[:4] == "the ":
            string = string[4:]
    string = string.replace("'", "").replace("th", "")
    date_list = re.split("[., /-]+", string)
    for i, word in enumerate(date_list):
        if len(word) and word[0].isdigit():
            date_list[i] = date_list[i].replace("st", "").replace("nd", "").replace("rd", "")
    if date_list[-1] == "":
        date_list.pop()
    date_string = " ".join(date_list)
    if re.fullmatch("[0-9]{1,5}(AD|BC|CE)", date_string):
        date_list = [date_string[:-2], date_string[-2:]]
        date_string = " ".join(date_list)
    weekday = day = month = year = ""
    opposite = False
    if len(date_list) == 1:
        if re.fullmatch("[0-9]{2,4}s?", date_list[0]):
            year = date_list[0]
    elif len(date_list) == 2:
        if re.fullmatch(months_re + " [0-9]{3,4}", date_string, flags=re.IGNORECASE):
            month = date_list[0]
            month = _parse_string_month(month)
            month = month.lower()
            year = date_list[1]
        elif re.fullmatch(months_re + " [0-9]{1,2}", date_string, flags=re.IGNORECASE):
            opposite = True
            month = date_list[0]
            month = _parse_string_month(month)
            month = month.lower()
            day = date_list[1]
        elif re.fullmatch("[0-9]{1,2} " + months_re, date_string, flags=re.IGNORECASE):
            month = date_list[1]
            month = _parse_string_month(month)
            month = month.lower()
            day = date_list[0]
        elif re.fullmatch("[0-9]{1,4} (BC|AD|CE|BCE)", date_string):
            year = _parse_year(date_list[0])
            if int(date_list[0]) < 10:
                year = "o " + year
            affix = parse_letters(date_list[1])
            return " ".join([year, affix])
    elif len(date_list) == 3:
        if re.fullmatch("[0-9]{1,2} " + months_re + " [0-9]{2,4}", date_string, flags=re.IGNORECASE):
            day = date_list[0]
            month = date_list[1]
            month = _parse_string_month(month)
            month = month.lower()
            year = date_list[2]
        elif re.fullmatch(months_re + " [0-9]{1,2} [0-9]{2,4}", date_string, flags=re.IGNORECASE):
            opposite = True
            day = date_list[1]
            month = date_list[0]
            month = _parse_string_month(month)
            month = month.lower()
            year = date_list[2]
        elif re.fullmatch("[0-9]{1,4} [0-9]{1,2} [0-9]{1,4}", date_string):
            if len(date_list[0]) == 4:
                day = date_list[2]
                month = date_list[1]
                month = _parse_num_month(month)
                year = date_list[0]
            else:
                if int(date_list[1]) <= 12:
                    day = date_list[0]
                    month = date_list[1]
                else:
                    opposite = True
                    day = date_list[1]
                    month = date_list[0]
                month = _parse_num_month(month)
                year = date_list[2]
        elif re.fullmatch("[0-9]{1,4} " + months_re + " [0-9]{1,4}", date_string, flags=re.IGNORECASE):
            if len(date_list[0]) == 4:
                day = date_list[2]
                month = date_list[1]
                month = _parse_string_month(month)
                year = date_list[0]
            else:
                day = date_list[0]
                year = date_list[2]
        elif re.fullmatch(weekdays_re + " " + months_re + " [0-9]{1,2}", date_string, flags=re.IGNORECASE):
            opposite = True
            weekday = date_list[0]
            weekday = _parse_weekday(weekday)
            day = date_list[2]
            month = date_list[1]
            month = _parse_string_month(month)
            month = month.lower()
        elif re.fullmatch(weekdays_re + " [0-9]{1,2} " + months_re, date_string, flags=re.IGNORECASE):
            weekday = date_list[0]
            weekday = _parse_weekday(weekday)
            day = date_list[1]
            month = date_list[2]
            month = _parse_string_month(month)
            month = month.lower()
        elif re.fullmatch("[0-9]{1,4} (B C|C E|A D)", date_string):
            return _parse_year(date_list[0]) + " " + " ".join(date_list[1:]).lower()
    elif len(date_list) == 4:
        if re.fullmatch(weekdays_re + " [0-9]{1,2} " + months_re + " [0-9]{3,4}", date_string, flags=re.IGNORECASE):
            weekday = date_list[0]
            weekday = _parse_weekday(weekday)
            day = date_list[1]
            month = date_list[2]
            month = _parse_string_month(month)
            month = month.lower()
            year = date_list[3]
        elif re.fullmatch(weekdays_re + " " + months_re + " [0-9]{1,2} [0-9]{3,4}", date_string, flags=re.IGNORECASE):
            opposite = True
            weekday = date_list[0]
            weekday = _parse_weekday(weekday)
            day = date_list[2]
            month = date_list[1]
            month = _parse_string_month(month)
            month = month.lower()
            year = date_list[3]
        elif re.fullmatch(weekdays_re + " [0-9]{1,4} [0-9]{1,2} [0-9]{1,4}", date_string, flags=re.IGNORECASE):
            weekday = date_list[0]
            weekday = _parse_weekday(weekday)
            if len(date_list[1]) == 4:
                day = date_list[3]
                month = date_list[2]
                month = _parse_num_month(month)
                year = date_list[1]
            else:
                if int(date_list[2]) <= 12:
                    day = date_list[1]
                    month = date_list[2]
                else:
                    opposite = True
                    day = date_list[2]
                    month = date_list[1]
                month = _parse_num_month(month)
                year = date_list[3]
    if len(year):
        year = _parse_year(year)
        if string[-1] == "s":
            if year[-1] == "y":
                year = year[:-1] + "ies"
            elif year[-1] == "x":
                year += "es"
            else:
                year += "s"

    if len(day):
        day = parse_ordinals(day)
    if len(weekday) and len(day) and len(month) and len(year):
        if opposite:
            return " ".join([weekday, month, day, year])
        else:
            return " ".join([weekday, "the", day, "of", month, year])
    elif len(day) and len(month) and len(year):
        if opposite:
            return " ".join([month, day, year])
        else:
            return " ".join(["the", day, "of", month, year])
    elif len(day) and len(month) and len(weekday):
        if opposite:
            return " ".join([weekday, month, day])
        else:
            return " ".join([weekday, "the", day, "of", month])
    elif len(month) and len(year):
        return " ".join([month, year])
    elif len(month) and len(day):
        if opposite:
            return " ".join([month, day])
        else:
            return " ".join(["the", day, "of", month])
    elif len(year):
        return year

    return string


def _parse_year(string: str):
    string = string.replace("'", "").replace("s", "")
    if len(string) == 1:
        return parse_cardinal(string)
    elif len(string) == 2:
        if string[0] == "0":
            return " ".join(["o", parse_cardinal(string[1])])
        else:
            return parse_cardinal(string)
    elif string[1:3] == "00":
        return parse_cardinal(string)
    elif string[-2:] == "00" and string[:2] not in ["10", "20"]:
        return " ".join([parse_cardinal(string[:-2]), "hundred"])
    elif string[1:3].isdigit():
        if string[-2] == "0":
            return " ".join([parse_cardinal(string[:-2]), "o", parse_cardinal(string[-1])])
        else:
            return " ".join([parse_cardinal(string[:-2]), parse_cardinal(string[-2:])])
    return string


def _parse_num_month(string: str):
    if int(string) in months_num_dict:
        return months_num_dict[int(string)]
    else:
        return "month_error"


def _parse_weekday(string: str):
    if string.lower() in weekdays_short_dict:
        return weekdays_short_dict[string.lower()]
    else:
        return string.lower()


def _parse_string_month(string: str):
    string = string[0].capitalize() + string[1:].lower()
    if string in months_short_dict:
        return months_short_dict[string]
    else:
        return string


def parse_decimal(string: str):
    try:
        number = ""
        if ".0" == string:
            number = "point o"
        elif "." in string:
            cardinal = ""
            if string[0] == ".":
                decimal = string[1:]
            else:
                cardinal, decimal = string.split(".")
                cardinal = parse_cardinal(cardinal)
            decimal = parse_digits(decimal)
            if decimal == "o":
                decimal = "zero"
            if len(cardinal) and len(decimal):
                number = " ".join([cardinal, "point", decimal])
            elif not len(cardinal):
                number = " ".join(["point", decimal])
        else:
            number = parse_cardinal(string)
        if "million" in string:
            number = number + " million"
        if "billion" in string:
            number = number + " billion"
        if "thousand" in string:
            number = number + " thousand"
        if "trillion" in string:
            number = number + " trillion"
        return number
    except:
        return string


digits_dict = {"0": "o", "1": "one", "2": "two", "3": "three",
               "4": "four", "5": "five", "6": "six", "7": "seven",
               "8": "eight", "9": "nine"}


def parse_digits(string):
    if string == "007":
        return "double o seven"
    after = []
    for char in string:
        if char in digits_dict:
            after.append(digits_dict[char])
    return " ".join(after)


electronic_digits_dict = {"0": "o", "1": " ".join("one"), "2": " ".join("two"), "3": " ".join("three"),
                          "4": " ".join("four"), "5": " ".join("five"), "6": " ".join("six"), "7": " ".join("seven"),
                          "8": " ".join("eight"), "9": " ".join("nine"), ",": "c o m m a", "~": "t i l d e",
                          "_": "u n d e r s c o r e"}


def parse_electronic(string: str):
    if string == "::":
        return string

    if re.fullmatch("[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", string):
        after = string.split(".")
        ip = []
        for x in after:
            if len(x) > 2:
                x = parse_digits(x)
                x = "".join(x.split())
            else:
                if x == "0":
                    x = "o"
                else:
                    x = parse_cardinal(x)
                x = x.replace(" ", "")
            x = " ".join(x)
            ip.append(x)
            ip.append("dot")
        ip.pop()
        ip = " ".join(ip)
        return ip

    if string[0] == "#" and len(string) > 1:
        return "hash tag " + string[1:].lower()

    if bool(re.fullmatch("\.[0-9][.][0-9A-Za-z-]+", string)):
        parsed_string = []
        for char in string:
            parsed_string.append(_electronic_parse_char_http(char))
        return " ".join(parsed_string)

    string = _electronic_find_numbers(string)
    after = []
    if string[:7] == "http://" and len(string) > 7:
        after.append("h t t p colon slash slash")
        for char in string[7:]:
            after.append(_electronic_parse_char_http(char))
        after = " ".join(after)
        after = after.replace("dot c o m", "dot com")
        return after

    if string[:8] == "https://" and len(string) > 8:
        after.append("h t t p s colon slash slash")
        for char in string[8:]:
            after.append(_electronic_parse_char_http(char))
        after = " ".join(after)
        after = after.replace("dot c o m", "dot com")
        return after

    for char in string:
        after.append(_electronic_parse_char(char))
    return " ".join(after)


def _electronic_parse_char(char):
    if char == "é":
        return "e a c u t e"
    elif char.isalpha():
        return char.lower()
    elif char == ".":
        return "dot"
    elif char == "+":
        return "plus"
    elif char == "/":
        return "s l a s h"
    elif char == "-":
        return "d a s h"
    elif char == "(":
        return "o p e n i n g p a r e n t h e s i s"
    elif char == ")":
        return "c l o s i n g p a r e n t h e s i s"
    elif char == ":":
        return "c o l o n"
    elif char == "%":
        return "p e r c e n t"
    elif char in electronic_digits_dict:
        return electronic_digits_dict[char]
    else:
        return char


def _electronic_parse_char_http(char):
    if char.isalpha():
        return char.lower()
    elif char == ".":
        return "dot"
    elif char == "/":
        return "slash"
    elif char == "-":
        return "dash"
    elif char == ":":
        return "colon"
    elif char == "(":
        return "o p e n i n g p a r e n t h e s i s"
    elif char == ")":
        return "c l o s i n g p a r e n t h e s i s"
    elif char == "'":
        return "s i n g l e q u o t e"
    elif char == "#":
        return "h a s h"
    elif char == "%":
        return "p e r c e n t"
    elif char in electronic_digits_dict:
        return electronic_digits_dict[char]
    else:
        return char


def _electronic_find_numbers(string: str):
    while len(re.findall("\d", string)):
        number = []
        number_i = -1
        for i, char in enumerate(string):
            if char in "0123456789":
                number.append(char)
                if number_i < 0:
                    number_i = i
            else:
                if len(number):
                    pre = string[:number_i]
                    post = string[i:]
                    number = _electronic_parse_numbers("".join(number))
                    string = pre + number + post
                    break
        if i == len(string) - 1:
            pre = string[:number_i]
            post = string[i + 1:]
            number = _electronic_parse_numbers("".join(number))
            string = pre + number + post
    return string


def _electronic_parse_numbers(string: str):
    if string[0] == "0":
        string = parse_digits(string)
    elif len(string) == 4:
        if int(string) < 2100:
            string = parse_date(string)
        else:
            string = parse_digits(string)
    elif len(string) == 2:
        string = parse_cardinal(string)
    elif len(string) > 4:
        string = parse_digits(string)
    elif len(string) < 4:
        string = parse_digits(string)

    string = "".join(string.split())

    return string


def parse_fraction(string: str):
    string = string.replace(
        "½", " 1/2").replace(
        "¾", " 3/4").replace(
        "¼", " 1/4").replace(
        "⅛", " 1/8").replace(
        "⅝", " 5/8").replace(
        "⅞", " 7/8").replace(
        "⅔", " 2/3").replace(
        "⅓", " 1/3")
    if string[0] == " ":
        string = string[1:]
    number = ""
    cardinal = ""
    if "/" in string:
        if "/" in string[1:]:
            numerator, denominator = string.split("/")

            if " " in numerator[:-1]:
                cardinal, numerator = numerator.split()
                cardinal = parse_cardinal(cardinal)
            numerator = parse_cardinal(numerator)
            denominator = parse_ordinals(denominator)

            if denominator == "second":
                if numerator in ["one", "minus one"]:
                    denominator = "half"
                else:
                    denominator = "halve"
            elif denominator == "fourth":
                denominator = "quarter"

            if len(cardinal):
                number += cardinal + " and "
                if numerator == "one":
                    numerator = "a"

            if denominator == "first":
                number += numerator + " over one"
            else:
                number += numerator + " " + denominator
                if numerator not in ["minus one", "one", "a"]:
                    number += "s"
            return number
    return string


def parse_letters(string):
    if string == "'":
        return string
    string = string.replace("'", "")
    after = []
    if len(string) == 1:
        if string == "é":
            return "e acute"
        else:
            return string
    for char in string:
        if char.isalpha():
            if char == "é":
                after.append("e acute")
            else:
                after.append(char.lower())
        elif char == "&":
            after.append("and")
    after = " ".join(after)
    if len(string) > 1:
        if (not string[:-1].islower()) and string[-1] == "s":
            after = after[:-2] + "'" + after[-1]
    return after


measure_number_string = "0123456789-,. "
measure_unit_df = pd.DataFrame.from_csv("outputs/units_v2.csv")
SUP = str.maketrans("⁰¹²³⁴⁵⁶⁷⁸⁹", "0123456789")


def parse_measure(string: str):
    if len(re.findall("[KM]iB|million|billion", string)):
        return string
    string = string.translate(SUP)
    unit = ""
    number = []
    for i, char in enumerate(string):
        if char in measure_number_string:
            number.append(char.lower())
        else:
            break
    for i, char in enumerate(string):
        if char not in measure_number_string:
            if char == "/" and i + 1 < len(string) and string[i + 1] in "0123456789":
                i += 1
                while i < len(string) and string[i] in "0123456789":
                    i += 1
                unit = string[i:].replace(" ", "")
                break
            else:
                unit = string[i:].replace(" ", "")
                break
    number = "".join(number)
    number.replace(" ", "")
    if len(unit) and len(number):
        if unit in measure_unit_df.index.values:
            unit = measure_unit_df["after"].at[unit]
        if "." in number:
            number = parse_decimal(number)
        else:
            number = parse_cardinal(number)
        if number == "one" and unit == "feet":
            return "one foot"
        if number == "one" and unit == "inches":
            return "one inch"
        if number == "one" and unit[-1] == "s":
            unit = unit[:-1]
        return " ".join([number, unit])

    elif len(unit):
        if unit in measure_unit_df.index.values:
            return measure_unit_df["after"].at[unit]
    return string


money_currencies_df = pd.DataFrame.from_csv("outputs/currencies.csv")
money_number_regex = "-?[0-9]+(,[0-9]{3})*(.[0-9]{1,3})? ?([Mm](illion)?|b(n|illion)?|k|trillion)?"


def strip_money_number(x: str):
    number = re.search(money_number_regex, x, flags=re.IGNORECASE)
    if number:
        number = _longest_match(number)
    return x.replace(number, "").replace(" ", "").replace(".", "")


def parse_money(string: str):
    number = re.search("[0-9]+(,[0-9]{3})*(.[0-9]{1,3})?", string)
    number_full = ""
    number_1 = ""
    number_2 = ""
    if number:
        number = _longest_match(number)
        number_full = parse_decimal(number)
        number = number.split(".")
        if len(number) > 1:
            number_1 = parse_cardinal(number[0])
            number_2 = number[1]
            if len(number_2) == 1:
                number_2 = number_2 + "0"
            number_2 = parse_cardinal(number_2)
        else:
            number_1 = parse_cardinal(number[0])

    currency = strip_money_number(string)
    if currency in money_currencies_df.index.values:
        currency = money_currencies_df["after"].at[currency]

    amount = None
    if bool(re.fullmatch(".*[0-9] ?([Mm](illion)?).*", string)):
        amount = "million"
    elif bool(re.fullmatch(".*[0-9] ?([Bb]n?(illion)?).*", string)):
        amount = "billion"
    elif bool(re.fullmatch(".*[0-9] ?([Tt](rillion)?).*", string)):
        amount = "trillion"
    elif bool(re.fullmatch(".*[0-9] ?([Kk]).*", string)):
        amount = "thousand"

    if currency and number_1 and amount is None:
        if number_1 == "zero" and len(number) > 1:
            return " ".join([number_2, "cents"])

        if number_1 == "one" and currency[-1] == "s":
            currency = currency[:-1]
        if len(number_2) and number_2 != "zero" and currency == "pounds":
            return " ".join([number_1, currency, "and", number_2, "pence"])
        if len(number_2) and number_2 != "zero":
            return " ".join([number_1, currency, "and", number_2, "cents"])
        else:
            return " ".join([number_1, currency])
    elif currency and number_1 and amount is not None:
        if number_2 == "zero":
            return " ".join([number_1, amount, currency])
        else:
            return " ".join([number_full, amount, currency])

    return string


def _longest_match(re_expression):
    index_start = -1
    index_end = -1
    length = 0
    for indices in re_expression.regs:
        if indices[0] >= 0:
            if length < indices[1] - indices[0]:
                length = indices[1] - indices[0]
                index_start = indices[0]
                index_end = indices[1]
    if length:
        return re_expression.string[index_start: index_end]
    else:
        return None


def parse_ordinals(string):
    is_roman = False
    is_s = False
    is_aps = False
    if len(string) > 1:
        if string[-2:] == "'s":
            is_aps = True
        elif string[-1] == "s":
            is_s = True
    if re_roman.match(string):
        is_roman = True
        string = str(_roman_to_int(string))
    after = []
    for char in string:
        if char in digits_set:
            after.append(char)
    if len(after):
        after = _ordinal_to_spoken("".join(after))
        if is_roman:
            after = " ".join(["the", after])
        elif string[0] == "-":
            after = "minus " + after
        if is_s:
            after += "s"
        elif is_aps:
            after += "'s"
        return after
    else:
        return string


def _ordinal_to_spoken(string):
    middle = num2words(int(string), ordinal=True)
    middle = middle.replace(",", "").replace("-", " ")
    middle = middle.split()
    after = []
    for word in middle:
        if word != "and":
            after.append(word)
    return " ".join(after)


phone_dict = {"0": "o", "1": "one", "2": "two", "3": "three",
              "4": "four", "5": "five", "6": "six", "7": "seven",
              "8": "eight", "9": "nine", " ": "sil"}


def parse_phone(string: str):
    string_list = re.split("[ ()x-]+", string)
    if string_list[0] == "":
        string_list = string_list[1:]
    if string_list[-1] == "":
        string_list = string_list[:-1]
    after = []
    for string in string_list:
        word = []
        if re.fullmatch("[01]?[1-9]0{2,3}", string):
            digits = ""
            if string[-3:] == "000":
                number = parse_cardinal(string[-4:])
                if len(string) > 4:
                    digits = string[:-4]
            else:
                number = parse_cardinal(string[-3:])
                if len(string) > 3:
                    digits = string[:-3]
            if len(digits):
                digits_word = []
                for char in digits:
                    if char in phone_dict:
                        digits_word.append(phone_dict[char])
                    elif char.isalpha():
                        digits_word.append(char.lower())
                digits_word = " ".join(digits_word)
                word = " ".join([digits_word, number])
            else:
                word = number
        else:
            for char in string:
                if char in phone_dict:
                    word.append(phone_dict[char])
                elif char.isalpha():
                    word.append(char.lower())
            word = " ".join(word)
        after.append(word)
        after.append("sil")
    after.pop()
    return " ".join(after)


plain_special_dict = {":": "to", "-": "to", "vol": "volume", "jr": "junior", "mt": "mount", "int'l": "international",
                      "vs": "versus", "st": "saint", "ltd": "limited", "dr": "doctor", "mr": "mister", "sq": "square",
                      "dept": "department", "cpl": "corporal", "rd": "road", "etc": "etcetera", "ok": "okay",
                      "elisabeth": "elizabeth", "risa": "riza", "pvt": "private", "bros": "brothers", "x ": "by",
                      "yr": "year", "elisa": "eliza", "prof": "professor", "isaak": "izaak", "wy": "way",
                      "louvre": "Louvre", "intl": "international", "okanagan": "okanogan"}
plain_months_short_dict = {"jan": "january", "feb": "february", "mar": "march", "apr": "april",
                           "jun": "june", "jul": "july", "aug": "august", "sep": "september",
                           "oct": "october", "nov": "november", "dec": "december"}

reer_dict = {}
mmem_dict = {}
short_dict = {}
gueg_dict = {}
isiz_dict = {}
orour_dict = {}


def build_plain_dictionaries(n):
    plain_todo_dict = plain_helpers.todo_dicter_v2(n)
    global reer_dict
    global mmem_dict
    global short_dict
    global gueg_dict
    global isiz_dict
    global orour_dict
    reer_dict = plain_helpers.reer_dicter(plain_todo_dict)
    mmem_dict = plain_helpers.mmem_dicter(plain_todo_dict)
    short_dict = plain_helpers.short_dicter(plain_todo_dict)
    gueg_dict = plain_helpers.gueg_dicter(plain_todo_dict)
    isiz_dict = plain_helpers.isiz_dicter(plain_todo_dict)
    orour_dict = plain_helpers.orour_dicter(plain_todo_dict)
    print(len(short_dict), short_dict)
    print(len(orour_dict), orour_dict)
    print(len(isiz_dict), isiz_dict)


def parse_plain(string: str):

    if string.lower() in plain_special_dict:
        return plain_special_dict[string.lower()]
    if len(string) > 7:
        if "strasse" in string:
            return string[:-7].lower() + " strasse"
    if len(string) > 4:
        if string[-3:] == "weg":
            return string[:-3].lower() + " weg"

    stem = stemmer.stem(string).lower()
    affix = ""
    if stem in reer_dict:
        if len(stem) < len(string) - 1:
            affix = string[len(stem) + 1:]
        if affix == "d":
            return string
        return "".join([stem[:-1], "er", affix])
    elif stem in isiz_dict:
        affix = string[len(stem):]
        return "".join([stem[:-1], "z", affix])
    elif stem in orour_dict:
        string = string.replace("our", "or").lower()
        return string
    elif stem in mmem_dict:
        if len(stem) < len(string) - 1:
            affix = string[len(stem) + 1:]
        return "".join([stem[:-1], affix])
    elif stem in gueg_dict:
        if len(stem) < len(string) + 1:
            affix = string[len(stem) + 1:]
        if affix == "ng":
            affix = "ing"
        return "".join([stem[:-1], affix])

    if string[-1] == "!":
        return string[:-1]
    return string


def parse_punct(string):
    return string


def parse_time(string: str):
    time = letters = ""

    if len(re.findall("[a-zA-Z]", string)):
        letters = parse_letters(string)
        for i, char in enumerate(string):
            if char.isalpha():
                time = string[:i]
                break
    else:
        time = string

    hour = minutes = seconds = mili = ""
    time_splited = re.split("\W+", time)
    if bool(re.fullmatch("\d{1,2}:\d{2}\.\d{1,2}", string)):
        minutes = parse_cardinal(time_splited[0])
        seconds = parse_cardinal(time_splited[1])
        mili = parse_cardinal(time_splited[2])
    elif bool(re.fullmatch("\d{1,2}:\d{2}:\d{2}", string)):
        hour = parse_cardinal(time_splited[0])
        minutes = parse_cardinal(time_splited[1])
        seconds = parse_cardinal(time_splited[2])
    else:
        if len(time_splited):
            hour = parse_cardinal(time_splited[0])
        if len(time_splited) > 1:
            minutes = parse_cardinal(time_splited[1])
            if minutes == "zero":
                minutes = ""
            if len(time_splited[1]):
                if time_splited[1][0] == "0" and len(minutes):
                    minutes = "o " + minutes
        if len(time_splited) > 2:
            seconds = parse_cardinal(time_splited[2])

    if len(minutes) and len(seconds) and len(mili):
        if minutes == "one":
            time = " ".join([minutes, "minute", seconds, "seconds and", mili, "milliseconds"])
        else:
            time = " ".join([minutes, "minutes", seconds, "seconds and", mili, "milliseconds"])
    elif len(hour) and len(minutes) and len(seconds):
        if hour == "one" and minutes == "one":
            time = " ".join([hour, "hour", minutes, "minute and", seconds, "seconds"])
        elif hour == "one":
            time = " ".join([hour, "hour", minutes, "minutes and", seconds, "seconds"])
        elif minutes == "one":
            time = " ".join([hour, "hours", minutes, "minute and", seconds, "seconds"])
        else:
            time = " ".join([hour, "hours", minutes, "minutes and", seconds, "seconds"])
    elif len(hour) and len(minutes):
        time = " ".join([hour, minutes])
    else:
        time = hour

    if not len(minutes) and not len(letters):
        if time_splited[0].isdigit():
            if int(time_splited[0]) > 12:
                letters = "hundred"
            else:
                letters = "o'clock"
        else:
            letters = "o'clock"

    if len(letters):
        after = " ".join([time, letters])
    else:
        after = time

    return after


def parse_verbatim(string, dictionary):
    if string == "C#":
        return "c hash"
    elif string in dictionary:
        return dictionary[string]
    else:
        return string
