import numpy as np
cimport numpy as np
import re

DTYPE = np.int
ctypedef np.int_t DTYPE_t

def create_context_valuecounts(string_arr):
    cdef str string
    cdef int i
    context_list = []
    for string in string_arr:
        for i in range(len(string)-1):
            context_list.append(string[i:i+2].lower())
    return context_list


def search_sorted_series(list string_list, list sorted_vc_index, np.ndarray sorted_vc_values):
    cdef int i, j
    cdef int n = len(sorted_vc_index)
    cdef np.ndarray values = np.zeros([len(string_list)], dtype=DTYPE)
    cdef bint found
    cdef str string
    for i, string in enumerate(string_list):
        string = remove_junc(string)
        found = False
        for j in range(n):
            if string == sorted_vc_index[j]:
                values[i] = sorted_vc_values[j]
                found = True
                break
        if not found:
            values[i] = 0
    return values
            # compare index to string
            # if found return value count. else return 0.


def remove_junc(str string):
    while len(string) > 1 and string[-1] in [",", "-", " "] and not bool(re.fullmatch("-+", string)):
        string = string[:-1]
    return string