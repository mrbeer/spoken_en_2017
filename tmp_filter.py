import pandas as pd
import re

from re_filters import full_match

train = pd.read_csv("inputs/en_train_amended.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)
train = train[["before", "after", "class"]]

test = pd.read_csv("inputs/en_test.csv")
test["before"] = test["before"].astype(str)
test = test[["before"]]

my_class = "FRACTION"
#
# weekdays_re = "((Mon(day)?|Tue(sday)?|Wed(nesday)?|Thu(rsday)?|Fri(day)?|Sat(urday)?|Sun(day)?).?,? )"
# months_re = "(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|June?|" + \
#             "July?|Aug(ust)?|Sept?(ember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\.?,?"
# months_num_dict = {1: "january", 2: "february", 3: "march", 4: "april",
#                    5: "may", 6: "june", 7: "july", 8: "august", 9: "september",
#                    10: "october", 11: "november", 12: "december"}
#
date_pattern = re.compile("-? ?[0-9]* ?[0-9,]+ ?/ ?[0-9,]+|.*[½⅓⅔¼¾⅛⅝⅞].*")
#
train["isLove"], test["isLove"] = full_match(
    train[["before", "class"]], test[["before"]], date_pattern, my_class)

df = train[train["class"] == my_class]
errors_df = df[df["isLove"] == 0]
print(errors_df)
