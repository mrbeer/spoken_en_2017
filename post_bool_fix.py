import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime


def fix_bool(train: pd.DataFrame, test: pd.DataFrame, before_re, verbose=False):
    train_bool_df = train[train["before"].map(lambda x: bool(re.fullmatch(before_re, x)))]
    test_bool_df = test[test["before"].map(lambda x: bool(re.fullmatch(before_re, x)))]

    def sum_errors(df):
        return np.sum(df["after"] != df["prediction"])

    print(sum_errors(train_bool_df))
    if verbose:
        print(train_bool_df["after"].value_counts())

    after_values = train_bool_df["after"].value_counts().index.values

    train_bool_df["bool_target"] = (train_bool_df["after"] == after_values[1]).astype(int)

    def print_sentences(sentences_id_arr, n):
        print("There are %d words" % sentences_id_arr.size)
        n = min(n, sentences_id_arr.size)
        for i in range(n):
            sentence = train[train["sentence_id"] == sentences_id_arr[i]]
            words = sentence["before"].values.tolist()
            words_after = sentence["after"].values.tolist()
            class_pred = sentence["class_pred"].values.astype(str).tolist()
            print(i)
            print(" ".join(words))
            print(" ".join(words_after))
            print(" ".join(class_pred))

    n_sentences = 10
    if verbose:
        print("\nPrinting transformed sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_bool_df["sentence_id"][train_bool_df["bool_target"] == 1].values, n_sentences)
        print("\nPrinting not transformed sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_bool_df["sentence_id"][train_bool_df["bool_target"] == 0].values, n_sentences)

    def neigh_classes(full_df, df):
        df["before_class"] = -1
        df["before2_class"] = -1
        df["after_class"] = -1
        df["after2_class"] = -1
        indices = df.index.values
        tmp = full_df[["class_pred", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 2 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 2, 1]:
                    df["before2_class"].iat[i] = tmp[index_i - 2, 0]
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    df["before_class"].iat[i] = tmp[index_i - 1, 0]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    df["after_class"].iat[i] = tmp[index_i + 1, 0]
            if index_i + 2 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 2, 1]:
                    df["after2_class"].iat[i] = tmp[index_i + 2, 0]
        return df

    def neigh_number(full_df, df):
        df["before_number"] = -1
        df["after_number"] = -1
        indices = df.index.values
        tmp = full_df[["before", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i - 1, 0]):
                        df["before_number"].iat[i] = int(tmp[index_i - 1, 0])
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i + 1, 0]):
                        df["after_number"].iat[i] = int(tmp[index_i + 1, 0])
        return df

    train_bool_df = neigh_classes(train, train_bool_df)
    test_bool_df = neigh_classes(test, test_bool_df)

    train_bool_df = neigh_number(train, train_bool_df)
    test_bool_df = neigh_number(test, test_bool_df)

    xg_train = xgb.DMatrix(
        train_bool_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class"]].values,
        label=train_bool_df["bool_target"].values)
    xg_test = xgb.DMatrix(
        test_bool_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class"]].values)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'binary:logistic'
    param['eval_metric'] = 'error'
    # scale weight of positive examples
    param['eta'] = 0.1
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4

    watchlist = [(xg_train, 'train')]
    num_round = 200
    bst = xgb.train(param, xg_train, num_round, watchlist)

    # get prediction
    pred_train = bst.predict(xg_train)
    pred_train = (pred_train > 0.5).astype(int)
    pred_test = bst.predict(xg_test)
    pred_test = (pred_test > 0.5).astype(int)

    train_bool_df["bool_pred"] = pred_train
    print(np.sum(train_bool_df["bool_pred"] != train_bool_df["bool_target"]))

    if verbose:
        print(train_bool_df[["before", "after", "prediction", "bool_target"]][train_bool_df["after"] !=
                                                                              train_bool_df["prediction"]])

        wrong_prediction = train_bool_df[train_bool_df["bool_pred"] != train_bool_df["bool_target"]]
        print("\nPrinting transformed sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["bool_target"] == 1].values, n_sentences)
        print("\nPrinting not transformed sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["bool_target"] == 0].values, n_sentences)

    print("Fixing")
    train_indices = train_bool_df.index.values
    for i, index_i in enumerate(train_indices):
        if pred_train[i]:
            train["prediction"].iat[index_i] = after_values[1]
        else:
            train["prediction"].iat[index_i] = after_values[0]
    test_indices = test_bool_df.index.values
    for i, index_i in enumerate(test_indices):
        if pred_test[i]:
            test["prediction"].iat[index_i] = after_values[1]
        else:
            test["prediction"].iat[index_i] = after_values[0]

    return train, test
