import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime

import word_parser


def fix_nums(train: pd.DataFrame, test: pd.DataFrame, evaluation: pd.DataFrame, verbose=False):
    train_num_df = train[train["before"].map(lambda x: bool(re.fullmatch("[0-9]+", x)))]
    eval_num_df = evaluation[evaluation["before"].map(lambda x: bool(re.fullmatch("[0-9]+", x)))]
    test_num_df = test[test["before"].map(lambda x: bool(re.fullmatch("[0-9]+", x)))]

    def sum_errors(df):
        return np.sum(df["after"] != df["prediction"])

    print(sum_errors(train_num_df))
    if verbose:
        print(train_num_df["class_pred"].value_counts())

    train_num_df["num_target"] = 0
    for i in range(train_num_df.shape[0]):
        if train_num_df["after"].iat[i] == word_parser.parse_cardinal(train_num_df["before"].iat[i]):
            train_num_df["num_target"].iat[i] = 0
        elif train_num_df["after"].iat[i] == word_parser.parse_digits(train_num_df["before"].iat[i]):
            train_num_df["num_target"].iat[i] = 1
        elif train_num_df["after"].iat[i] == word_parser.parse_date(train_num_df["before"].iat[i]):
            train_num_df["num_target"].iat[i] = 2
    eval_num_df["num_target"] = 0
    for i in range(eval_num_df.shape[0]):
        if eval_num_df["after"].iat[i] == word_parser.parse_cardinal(eval_num_df["before"].iat[i]):
            eval_num_df["num_target"].iat[i] = 0
        elif eval_num_df["after"].iat[i] == word_parser.parse_digits(eval_num_df["before"].iat[i]):
            eval_num_df["num_target"].iat[i] = 1
        elif eval_num_df["after"].iat[i] == word_parser.parse_date(eval_num_df["before"].iat[i]):
            eval_num_df["num_target"].iat[i] = 2

    if verbose:
        print(train_num_df["num_target"].value_counts())

    def print_sentences(sentences_id_arr, n):
        print("There are %d words" % sentences_id_arr.size)
        n = min(n, sentences_id_arr.size)
        for i in range(n):
            sentence = train[train["sentence_id"] == sentences_id_arr[i]]
            words = sentence["before"].values.tolist()
            words_after = sentence["after"].values.tolist()
            class_pred = sentence["class_pred"].values.astype(str).tolist()
            print(i)
            print(" ".join(words))
            print(" ".join(words_after))
            print(" ".join(class_pred))

    n_sentences = 10
    if verbose:
        print("\nPrinting 2 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_num_df["sentence_id"][train_num_df["num_target"] == 2].values, n_sentences)
        print("\nPrinting 1 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_num_df["sentence_id"][train_num_df["num_target"] == 1].values, n_sentences)
        print("\nPrinting 0 sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(train_num_df["sentence_id"][train_num_df["num_target"] == 0].values, n_sentences)

    def neigh_classes(full_df, df):
        df["before_class"] = -1
        df["before2_class"] = -1
        df["after_class"] = -1
        df["after2_class"] = -1
        indices = df.index.values
        tmp = full_df[["class_pred", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 2 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 2, 1]:
                    df["before2_class"].iat[i] = tmp[index_i - 2, 0]
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    df["before_class"].iat[i] = tmp[index_i - 1, 0]
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    df["after_class"].iat[i] = tmp[index_i + 1, 0]
            if index_i + 2 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 2, 1]:
                    df["after2_class"].iat[i] = tmp[index_i + 2, 0]
        return df

    def neigh_number(full_df, df):
        df["before_number"] = -1
        df["after_number"] = -1
        indices = df.index.values
        tmp = full_df[["before", "sentence_id"]].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i, 1] == tmp[index_i - 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i - 1, 0]):
                        df["before_number"].iat[i] = int(tmp[index_i - 1, 0])
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i, 1] == tmp[index_i + 1, 1]:
                    if re.fullmatch("-?[0-9]+", tmp[index_i + 1, 0]):
                        df["after_number"].iat[i] = int(tmp[index_i + 1, 0])
        return df

    def before_x(full_df, df):
        pre_cardinal_series = pd.Series.from_csv("outputs/before_cardinals.csv")
        pre_cardinal_index = pre_cardinal_series.index.values
        after_cardinal_series = pd.Series.from_csv("outputs/after_cardinals.csv")
        after_cardinal_index = after_cardinal_series.index.values

        pre_ordinal_series = pd.Series.from_csv("outputs/before_ordinals.csv")
        pre_ordinal_index = pre_ordinal_series.index.values
        after_ordinal_series = pd.Series.from_csv("outputs/after_ordinals.csv")
        after_ordinal_index = after_ordinal_series.index.values

        pre_date_series = pd.Series.from_csv("outputs/before_date.csv")
        pre_date_index = pre_date_series.index.values
        after_date_series = pd.Series.from_csv("outputs/after_date.csv")
        after_date_index = after_date_series.index.values

        pre_digit_series = pd.Series.from_csv("outputs/before_digit.csv")
        pre_digit_index = pre_digit_series.index.values
        after_digit_series = pd.Series.from_csv("outputs/after_digit.csv")
        after_digit_index = after_digit_series.index.values

        df["before_cardinal_freq"] = 0
        df["after_cardinal_freq"] = 0
        df["before_ordinal_freq"] = 0
        df["after_ordinal_freq"] = 0
        df["before_date_freq"] = 0
        df["after_date_freq"] = 0

        df["before_digit_freq"] = 0
        df["before_digit_sil"] = 0
        df["after_digit_freq"] = 0
        indices = df.index.values
        tmp = full_df["before"].values
        for i, index_i in enumerate(indices):
            if index_i - 1 >= 0:
                if tmp[index_i - 1].lower() in pre_cardinal_index:
                    df["before_cardinal_freq"].iat[i] = pre_cardinal_series[tmp[index_i - 1].lower()]
                if tmp[index_i - 1].lower() in pre_ordinal_index:
                    df["before_ordinal_freq"].iat[i] = pre_ordinal_series[tmp[index_i - 1].lower()]
                if tmp[index_i - 1].lower() in pre_date_index:
                    df["before_date_freq"].iat[i] = pre_date_series[tmp[index_i - 1].lower()]
                if tmp[index_i - 1].lower() in pre_digit_index:
                    df["before_digit_freq"].iat[i] = pre_digit_series[tmp[index_i - 1].lower()]
                if bool(re.fullmatch("[a-z]+-", tmp[index_i - 1].lower())):
                    df["before_digit_sil"].iat[i] = 1
            if index_i + 1 < tmp.shape[0]:
                if tmp[index_i + 1].lower() in after_cardinal_index:
                    df["after_cardinal_freq"].iat[i] = after_cardinal_series[tmp[index_i + 1].lower()]
                if tmp[index_i + 1].lower() in after_ordinal_index:
                    df["after_ordinal_freq"].iat[i] = after_ordinal_series[tmp[index_i + 1].lower()]
                if tmp[index_i + 1].lower() in after_date_index:
                    df["after_date_freq"].iat[i] = after_date_series[tmp[index_i + 1].lower()]
                if tmp[index_i + 1].lower() in after_digit_index:
                    df["after_digit_freq"].iat[i] = after_digit_series[tmp[index_i + 1].lower()]
        df["before_ordinal_cardinal_diff"] = df["before_cardinal_freq"].values - df["before_ordinal_freq"].values
        df["after_ordinal_cardinal_diff"] = df["after_cardinal_freq"].values - df["after_ordinal_freq"].values
        return df

    train_num_df = neigh_classes(train, train_num_df)
    eval_num_df = neigh_classes(evaluation, eval_num_df)
    test_num_df = neigh_classes(test, test_num_df)

    train_num_df = before_x(train, train_num_df)
    eval_num_df = before_x(evaluation, eval_num_df)
    test_num_df = before_x(test, test_num_df)

    train_num_df = neigh_number(train, train_num_df)
    eval_num_df = neigh_number(evaluation, eval_num_df)
    test_num_df = neigh_number(test, test_num_df)

    def num_values(df):
        df["value"] = 999999
        for i in range(df.shape[0]):
            if len(df["before"].iat[i]) < 8:
                df["value"].iat[i] = int(df["before"].iat[i])
        return df

    train_num_df = num_values(train_num_df)
    eval_num_df = num_values(eval_num_df)
    test_num_df = num_values(test_num_df)

    train_num_df["length"] = train_num_df["before"].map(lambda x: len(x))
    eval_num_df["length"] = eval_num_df["before"].map(lambda x: len(x))
    test_num_df["length"] = test_num_df["before"].map(lambda x: len(x))

    train_num_df["div2"] = train_num_df["before"].map(lambda x: int(bool(int(x) % 10)))
    eval_num_df["div2"] = eval_num_df["before"].map(lambda x: int(bool(int(x) % 10)))
    test_num_df["div2"] = test_num_df["before"].map(lambda x: int(bool(int(x) % 10)))

    train_num_df["div3"] = train_num_df["before"].map(lambda x: int(bool(int(x) % 100)))
    eval_num_df["div3"] = eval_num_df["before"].map(lambda x: int(bool(int(x) % 100)))
    test_num_df["div3"] = test_num_df["before"].map(lambda x: int(bool(int(x) % 100)))

    train_num_df["div4"] = train_num_df["before"].map(lambda x: int(bool(int(x) % 1000)))
    eval_num_df["div4"] = eval_num_df["before"].map(lambda x: int(bool(int(x) % 1000)))
    test_num_df["div4"] = test_num_df["before"].map(lambda x: int(bool(int(x) % 1000)))

    train_num_df["div"] = train_num_df["before"].map(lambda x: int(x) % 10)
    eval_num_df["div"] = eval_num_df["before"].map(lambda x: int(x) % 10)
    test_num_df["div"] = test_num_df["before"].map(lambda x: int(x) % 10)

    train_num_df["zero_start"] = train_num_df["before"].map(lambda x: int(x[0] == "0"))
    eval_num_df["zero_start"] = eval_num_df["before"].map(lambda x: int(x[0] == "0"))
    test_num_df["zero_start"] = test_num_df["before"].map(lambda x: int(x[0] == "0"))

    train_indices = train_num_df.index.values
    eval_indices = eval_num_df.index.values
    test_indices = test_num_df.index.values
    train_num_df["isPreName"] = 0
    eval_num_df["isPreName"] = 0
    test_num_df["isPreName"] = 0
    for i in range(1, train_num_df.shape[0]):
        if bool(re.fullmatch("[A-Z][a-z]+", train["before"].at[train_indices[i] - 1])):
            train_num_df["isPreName"].iat[i] = 1
    for i in range(1, eval_num_df.shape[0]):
        if bool(re.fullmatch("[A-Z][a-z]+", evaluation["before"].at[eval_indices[i] - 1])):
            eval_num_df["isPreName"].iat[i] = 1
    for i in range(1, test_num_df.shape[0]):
        if bool(re.fullmatch("[A-Z][a-z]+", test["before"].at[test_indices[i] - 1])):
            test_num_df["isPreName"].iat[i] = 1

    train_num_df["isPreLetters"] = 0
    eval_num_df["isPreLetters"] = 0
    test_num_df["isPreLetters"] = 0
    for i in range(1, train_num_df.shape[0]):
        if bool(re.fullmatch("[A-Z]+-?", train["before"].at[train_indices[i] - 1])):
            train_num_df["isPreLetters"].iat[i] = 1
    for i in range(1, eval_num_df.shape[0]):
        if bool(re.fullmatch("[A-Z]+-?", evaluation["before"].at[eval_indices[i] - 1])):
            eval_num_df["isPreLetters"].iat[i] = 1
    for i in range(1, test_num_df.shape[0]):
        if bool(re.fullmatch("[A-Z]+-?", test["before"].at[test_indices[i] - 1])):
            test_num_df["isPreLetters"].iat[i] = 1

    train_num_df["isPreIn"] = 0
    eval_num_df["isPreIn"] = 0
    test_num_df["isPreIn"] = 0
    for i in range(1, train_num_df.shape[0]):
        if train["before"].at[train_indices[i] - 1] == "in":
            train_num_df["isPreIn"].iat[i] = 1
    for i in range(1, eval_num_df.shape[0]):
        if evaluation["before"].at[eval_indices[i] - 1] == "in":
            eval_num_df["isPreIn"].iat[i] = 1
    for i in range(1, test_num_df.shape[0]):
        if test["before"].at[test_indices[i] - 1] == "in":
            test_num_df["isPreIn"].iat[i] = 1

    xg_train = xgb.DMatrix(
        train_num_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "value",
                      "length", "zero_start", "isPreName", "isPreLetters", "isPreIn", "before_digit_sil",
                      "before_cardinal_freq", "before_ordinal_freq", "before_ordinal_cardinal_diff",
                      "after_cardinal_freq", "after_ordinal_freq", "after_ordinal_cardinal_diff",
                      "before_date_freq", "after_date_freq", "before_digit_freq", "after_digit_freq",
                      "div2", "div3", "div4", "div"]].values,
        label=train_num_df["num_target"].values)
    xg_eval = xgb.DMatrix(
        eval_num_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "value",
                     "length", "zero_start", "isPreName", "isPreLetters", "isPreIn", "before_digit_sil",
                     "before_cardinal_freq", "before_ordinal_freq", "before_ordinal_cardinal_diff",
                     "after_cardinal_freq", "after_ordinal_freq", "after_ordinal_cardinal_diff",
                     "before_date_freq", "after_date_freq", "before_digit_freq", "after_digit_freq",
                     "div2", "div3", "div4", "div"]].values,
        label=eval_num_df["num_target"].values)
    xg_test = xgb.DMatrix(
        test_num_df[["before_class", "before2_class", "after_class", "class_pred", "after2_class", "value",
                     "length", "zero_start", "isPreName", "isPreLetters", "isPreIn", "before_digit_sil",
                     "before_cardinal_freq", "before_ordinal_freq", "before_ordinal_cardinal_diff",
                     "after_cardinal_freq", "after_ordinal_freq", "after_ordinal_cardinal_diff",
                     "before_date_freq", "after_date_freq", "before_digit_freq", "after_digit_freq",
                     "div2", "div3", "div4", "div"]].values)

    # setup parameters for xgboost
    param = {}
    # use softmax multi-class classification
    param['objective'] = 'multi:softmax'
    param['eval_metric'] = 'merror'
    # scale weight of positive examples
    param['eta'] = 0.03
    param['max_depth'] = 5
    param['silent'] = 1
    param['nthread'] = 4
    param['num_class'] = 3

    watchlist = [(xg_train, 'train'), (xg_eval, 'eval')]
    num_round = 1300
    bst = xgb.train(param, xg_train, num_round, watchlist)

    # get prediction
    pred_train = bst.predict(xg_train)
    pred_test = bst.predict(xg_test)

    print(np.sum(pred_train != train_num_df["num_target"]))

    if verbose:
        print(train_num_df[["before", "after", "prediction", "num_target"]][pred_train !=
                                                                            train_num_df["num_target"]])

        wrong_prediction = train_num_df[pred_train != train_num_df["num_target"]]
        print("\nPrinting date sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["num_target"] == 2].values, n_sentences)
        print("\nPrinting digits sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["num_target"] == 1].values, n_sentences)
        print("\nPrinting cardinal sentences", datetime.now().strftime("%H:%M:%S"))
        print_sentences(wrong_prediction["sentence_id"][wrong_prediction["num_target"] == 0].values, n_sentences)

    print("Fixing")
    for i, index_i in enumerate(train_indices):
        if pred_train[i] == 2:
            train["prediction"].iat[index_i] = word_parser.parse_date(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 2
        elif pred_train[i] == 1:
            train["prediction"].iat[index_i] = word_parser.parse_digits(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 4
        else:
            train["prediction"].iat[index_i] = word_parser.parse_cardinal(train["before"].iat[index_i])
            train["class_pred"].iat[index_i] = 1
    for i, index_i in enumerate(test_indices):
        if pred_test[i] == 2:
            test["prediction"].iat[index_i] = word_parser.parse_date(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 2
        elif pred_test[i] == 1:
            test["prediction"].iat[index_i] = word_parser.parse_digits(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 4
        else:
            test["prediction"].iat[index_i] = word_parser.parse_cardinal(test["before"].iat[index_i])
            test["class_pred"].iat[index_i] = 1

    return train, test

# 969
