import pandas as pd
import numpy as np
import xgboost as xgb
import re
from datetime import datetime

train = pd.DataFrame.from_csv("outputs/train.2.19.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)
train["prediction"] = train["prediction"].astype(str)

test = pd.DataFrame.from_csv("outputs/train.2.19.csv")
test["before"] = test["before"].astype(str)
test["prediction"] = test["prediction"].astype(str)

train_num_df = train[train["class"] == "VERBATIM"]
test_num_df = test[test["class"] == "VERBATIM"]


def sum_errors(df):
    print(df[df["after"] != df["prediction"]])
    return np.sum(df["after"] != df["prediction"])


print(sum_errors(train_num_df))
# print(train_num_df["class"].value_counts())
# print(test_num_df["class_pred"].value_counts())
#
# class_dict = {"DATE": 0, "CARDINAL": 1, "DIGIT": 2, "TELEPHONE": 3, "DECIMAL": 4, "VERBATIM": 5}
# train_num_df["target_class"] = (train_num_df["class"].map(lambda x: class_dict[x])).astype(int)
#
#
# def print_sentences(sentences_id_arr, n):
#     print("There are %d sentences" % sentences_id_arr.size)
#     n = min(n, sentences_id_arr.size)
#     for i in range(n):
#         sentence = train[train["sentence_id"] == sentences_id_arr[i]]
#         words = sentence["before"].values.tolist()
#         words_after = sentence["after"].values.tolist()
#         class_pred = sentence["class_pred"].values.astype(str).tolist()
#         print(i)
#         print(" ".join(words))
#         print(" ".join(words_after))
#         print(" ".join(class_pred))
#
#
# n_sentences = 10
# print("\nPrinting 0 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(train_num_df["sentence_id"][train_num_df["target_class"] == 0].values, n_sentences)
# print("\nPrinting 1 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(train_num_df["sentence_id"][train_num_df["target_class"] == 1].values, n_sentences)
# print("\nPrinting 2 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(train_num_df["sentence_id"][train_num_df["target_class"] == 2].values, n_sentences)
# print("\nPrinting 3 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(train_num_df["sentence_id"][train_num_df["target_class"] == 3].values, n_sentences)
# print("\nPrinting 4 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(train_num_df["sentence_id"][train_num_df["target_class"] == 4].values, n_sentences)
#
#
# def neigh_classes(full_df, df):
#     df["before_class"] = -1
#     df["after_class"] = -1
#     indices = df.index.values
#     tmp = full_df[["class_pred", "sentence_id"]].values
#     for i, index_i in enumerate(indices):
#         if index_i - 1 >= 0:
#             if tmp[index_i, 1] == tmp[index_i - 1, 1]:
#                 df["before_class"].iat[i] = tmp[index_i - 1, 0]
#         if index_i + 1 < tmp.shape[0]:
#             if tmp[index_i, 1] == tmp[index_i + 1, 1]:
#                 df["after_class"].iat[i] = tmp[index_i + 1, 0]
#     return df
#
#
# def hash_before(full_df, df):
#     df["hash_before"] = 0
#     indices = df.index.values
#     tmp = full_df[["before", "sentence_id"]].values
#     for i, index_i in enumerate(indices):
#         if index_i - 1 >= 0:
#             if tmp[index_i, 1] == tmp[index_i - 1, 1] and tmp[index_i - 1, 0] == "#":
#                 df["hash_before"].iat[i] = 1
#         if index_i - 2 >= 0:
#             if tmp[index_i, 1] == tmp[index_i - 2, 1] and tmp[index_i - 2, 0] == "#":
#                 df["hash_before"].iat[i] = 1
#     return df
#
#
# def feature_generator(full_df, df):
#     df = hash_before(full_df, df)
#     df["number"] = df["before"].map(lambda x: string_to_int(x))
#     df["startZero"] = df["before"].map(lambda x: int(x[0] == "0"))
#     return df
#
#
# def string_to_int(x):
#     try:
#         return int(x)
#     except:
#         return -999
#
#
# train_num_df = feature_generator(train, train_num_df)
# print(np.sum(train_num_df["hash_before"]))
# test_num_df = feature_generator(test, test_num_df)
#
# print(train_num_df)
#
# xg_train = xgb.DMatrix(
#     train_num_df[["number", "startZero"]].values,
#     label=train_num_df["target_class"].values)
# xg_test = xgb.DMatrix(
#     test_num_df[["number", "startZero"]].values)
#
# # setup parameters for xgboost
# param = {}
# # use softmax multi-class classification
# param['objective'] = 'multi:softmax'
# param['eval_metric'] = 'merror'
# # scale weight of positive examples
# param['eta'] = 0.1
# param['max_depth'] = 5
# param['silent'] = 1
# param['nthread'] = 4
# param['num_class'] = 16
#
# watchlist = [(xg_train, 'train')]
# num_round = 60
# bst = xgb.train(param, xg_train, num_round, watchlist)
# # get prediction
# pred_train = bst.predict(xg_train)
# pred_test = bst.predict(xg_test)
#
# print(np.sum(pred_train != train_num_df["target_class"].values))
#
# wrong_prediction = train_num_df[pred_train != train_num_df["target_class"].values]
# n_sentences = 10
# print("\nPrinting 0 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(wrong_prediction["sentence_id"][wrong_prediction["target_class"] == 0].values, n_sentences)
# print("\nPrinting 1 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(wrong_prediction["sentence_id"][wrong_prediction["target_class"] == 1].values, n_sentences)
# print("\nPrinting 2 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(wrong_prediction["sentence_id"][wrong_prediction["target_class"] == 2].values, n_sentences)
# print("\nPrinting 3 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(wrong_prediction["sentence_id"][wrong_prediction["target_class"] == 3].values, n_sentences)
# print("\nPrinting 4 sentences", datetime.now().strftime("%H:%M:%S"))
# print_sentences(wrong_prediction["sentence_id"][wrong_prediction["target_class"] == 4].values, n_sentences)
