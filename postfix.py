import pandas as pd
import re
from datetime import datetime
import numpy as np

from post_to_fix import fix_to
from post_bool_fix import fix_bool
from post_num_fix import fix_nums
from post_roman_fix import fix_romans
from post_XXX_fix import fix_caps
from post_Xxxx_fix import fix_names
from post_xXxx_fix import fix_caps_and_lowers
from post_xxx_fix import fix_lowers
from post_st_fix import fix_st
import word_parser

script_version = "4.3"
post_version = "23"
print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("outputs/train.{0}.csv".format(script_version))
train["before"] = train["before"].astype(str)
train["prediction"] = train["prediction"].astype(str)
train["after"] = train["after"].astype(str)
train["class_pred"] = train["class_pred"].astype(float).astype(int)

evaluation = pd.read_csv("outputs/en_evaluation.csv".format(script_version))
evaluation["before"] = evaluation["before"].astype(str)
evaluation["prediction"] = evaluation["prediction"].astype(str)
evaluation["after"] = evaluation["after"].astype(str)
evaluation["class_pred"] = evaluation["class_pred"].astype(float).astype(int)

test = pd.read_csv("outputs/test.{0}.csv".format(script_version), dtype=str)
test["before"] = test["before"].astype(str)
test["prediction"] = test["prediction"].astype(str)
test["class_pred"] = test["class_pred"].astype(float).astype(int)

print("Calculating plain dictionaries ", datetime.now().strftime("%H:%M:%S"))
word_parser.build_plain_dictionaries(1000000000)

print("Post processing ", datetime.now().strftime("%H:%M:%S"))

for i, before in enumerate(train["before"].values):
    if "&" in before and len(before) > 1:
        train["prediction"].iat[i] = word_parser.parse_letters(before)
        train["class_pred"].iat[i] = 7
for i, before in enumerate(test["before"].values):
    if "&" in before and len(before) > 1:
        test["prediction"].iat[i] = word_parser.parse_letters(before)
        test["class_pred"].iat[i] = 7

print("Post processing [A-Z][A-Z']*s?[ ,-]* ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_caps(train, test, evaluation, verbose=False)

print("Post processing [A-Z][a-z'é]+ ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_names(train, test, evaluation, verbose=False)

print("Post processing [A-Z]*[a-zé']+[A-Z']+[a-zéA-Z']*", datetime.now().strftime("%H:%M:%S"))
train, test = fix_caps_and_lowers(train, test, evaluation, verbose=False)

print("Post processing [a-zé']+ ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_lowers(train, test, evaluation, verbose=False)

print("Post processing to ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_to(train, test, evaluation)

print("Post processing No ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_bool(train, test, "No")
train, test = fix_bool(train, test, "no")

print("Post processing # ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_bool(train, test, "#")

print("Post processing Sun ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_bool(train, test, "Sun")

print("Post processing Col ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_bool(train, test, "Col", verbose=False)

print("Post processing x ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_bool(train, test, "x", verbose=False)

print("Post processing st", datetime.now().strftime("%H:%M:%S"))
train, test = fix_st(train, test)

print("Post processing [0-9]+ ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_nums(train, test, evaluation, verbose=False)

print("Post processing romans ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_romans(train, test, evaluation, verbose=False)

print("Post processing to ", datetime.now().strftime("%H:%M:%S"))
train, test = fix_to(train, test, evaluation)

specials = {"sr": "senior", "SR": "senior", "Québec": "quebec", "PLoS": "PLoS", "cir": "circle", "blvd": "boulevard",
            "w/": "with", "NASA's": "NASA's", "barbecue": "barbeque", "kg": "kilogram", "PL": "place", "lb": "pound",
            "LB": "pound", "pl": "place", "ave": "avenue", "S.H.I.E.L.D.": "shield", "nov": "november",
            "U.N.C.L.E.": "uncle", "Oct": "october", "1\"": "one inch", "Barbecue": "barbeque", ":(": "sad face sil"}
for i, before in enumerate(train["before"].values):
    if before in specials:
        train["prediction"].iat[i] = specials[before]
for i, before in enumerate(test["before"].values):
    if before in specials:
        test["prediction"].iat[i] = specials[before]

for i, before in enumerate(train["before"].values):
    if bool(re.match("advertisement|television|programmer|programmable|merchandise|" +
                             "circumcision|calibration|franchise'?s|synagogues|catalogued", before,
                     flags=re.IGNORECASE)):
        train["prediction"].iat[i] = before
for i, before in enumerate(test["before"].values):
    if bool(re.match("advertisement|television|programmer|programmable|merchandise|" +
                             "circumcision|calibration|franchise'?s|synagogues|catalogued", before,
                     flags=re.IGNORECASE)):
        test["prediction"].iat[i] = before

e_ecute = pd.read_csv("outputs/e_ecute.csv", names=["before", "after"])
e_ecute_dict = {}
for value in np.unique(e_ecute["before"].values):
    e_ecute_dict[value] = e_ecute["after"][e_ecute["before"] == value].iat[0]
print(e_ecute_dict)
for i, before in enumerate(train["before"].values):
    if before in e_ecute_dict:
        train["prediction"].iat[i] = e_ecute_dict[before]
for i, before in enumerate(test["before"].values):
    if before in e_ecute_dict:
        test["prediction"].iat[i] = e_ecute_dict[before]

print("Saving submit ", datetime.now().strftime("%H:%M:%S"))

submission = pd.DataFrame.from_csv("inputs/en_sample_submission_2.csv")
submission["after"] = test["prediction"].values
submission.to_csv("outputs/v{0}.{1}.csv".format(script_version, post_version), encoding="UTF-8")

tmp_df = train[["before", "after", "prediction", "class", "class_pred"]][train["after"] != train["prediction"]]
tmp_df.to_csv("outputs/not_solved_{0}.csv".format(post_version), encoding="UTF-8")
print("Done ", datetime.now().strftime("%H:%M:%S"))
