import pandas as pd
import numpy as np
from datetime import datetime

import word_parser

SESSION = "live"

print("Reading files ", datetime.now().strftime("%H:%M:%S"))
train = pd.read_csv("inputs/en_train.csv")
train["before"] = train["before"].astype(str)
train["after"] = train["after"].astype(str)

print(train["class"].value_counts())


def amend_to_digits(x, x_result):
    if word_parser.parse_digits(x) == x_result:
        return True
    else:
        return False


def amend_to_plain(x, x_result):
    if x == x_result and len(x) > 1:
        return True
    else:
        return False


def amend_to_letters(x: str, x_result):
    after = []
    for char in x:
        if char.isalpha():
            after.append(char.lower())
    after = " ".join(after)
    if after == x_result and len(x) > 1:
        return True
    else:
        return False


def amend_to_electronic(x: str, x_result):
    if word_parser.parse_electronic(x) == x_result and len(x) > 1:
        return True
    else:
        return False


for i, row in enumerate(train[["before", "after", "class"]].values):
    if row[2] in ["VERBATIM", "TELEPHONE", "CARDINAL"] and amend_to_digits(row[0], row[1]):
        print("%d, changing %s class to DIGIT" % (i, row[0]))
        train["class"].iat[i] = "DIGIT"
    elif row[2] == "VERBATIM" and amend_to_plain(row[0], row[1]):
        print("%d, changing %s class to PLAIN" % (i, row[0]))
        train["class"].iat[i] = "PLAIN"
    elif row[2] == "VERBATIM" and amend_to_letters(row[0], row[1]):
        print("%d, changing %s class to LETTERS" % (i, row[0]))
        train["class"].iat[i] = "LETTERS"
    elif row[2] == "VERBATIM" and amend_to_electronic(row[0], row[1]):
        print("%d, changing %s class to ELECTRONIC" % (i, row[0]))
        train["class"].iat[i] = "ELECTRONIC"

tmp = train[train["class"] == "VERBATIM"]
tmp = tmp[train["before"].map(lambda x: len(x) > 1)]
print(tmp)
print(tmp["before"].value_counts())

print(train["class"].value_counts())

train.index = train["sentence_id"]
del train["sentence_id"]
train.to_csv("inputs/en_train_amended.csv", encoding="UTF-8")
